#!/usr/bin/env python
# coding: utf-8

# In[ ]:





# In[9]:


import os
import ast
cwd = os.getcwd()

templateFiles = ["/0.org/U.template",
    "/constant/transportProperties.template",
    "/system/blockMeshDict.template",
    "/system/controlDict.template",
    "/system/setAlphaFieldDict.template" ]

parameterFile = cwd + '/system/parametersDict'
calc_lines = open(parameterFile).readlines()
value = 0
for line in calc_lines:
    if '#calc' not in line:
        continue
    #print (line)
    expression = line.split("\"")[1]
    replaceableVariable = '$' + line.split()[0]
    #print ( type(replaceableVariable))
        #print ((expression))
    value = str(eval(expression))
    #print(type(value))

    for id, file in enumerate (templateFiles):
         fileName = cwd + templateFiles[id]
         fileName = fileName[:-9]
         #print(fileName)
         get_ipython().system(" sed -i 's/{replaceableVariable}/{value}/g' {fileName}")


# In[ ]:





# In[ ]:





# In[ ]:




