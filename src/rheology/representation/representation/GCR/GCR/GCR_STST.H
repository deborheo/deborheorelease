/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

InNamespace
    Foam::GCR

SourceFiles
    GCR.C

Author
    Matthias Niethammer <niethammer@gmx.de>

Description
    GCR namespace symmTensor template specializations.

\*---------------------------------------------------------------------------*/

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace GCR
{

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#define COMMA ,

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#define GCRFUNC2(func)                                                       \
func<symmTensor COMMA tensor COMMA symmTensor>

#define GCRFUNC3(func)                                                       \
func<symmTensor COMMA tensor COMMA symmTensor COMMA scalar>

#define GCRFUNCP2(func)                                                      \
func<symmTensor COMMA symmTensor>

#define GCRFUNCP3(func)                                                      \
func<symmTensor COMMA symmTensor COMMA scalar>


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#define transformFunc(func)                                                 \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        sT.xx()*sT.xx(),                                                    \
        .0,                                                                 \
        .0,                                                                 \
        sT.yy()*sT.yy(),                                                    \
        .0,                                                                 \
        sT.zz()*sT.zz()                                                     \
    );                                                                      \
}
transformFunc(gsqr)
#undef transformFunc

#define transformFunc(func)                                                 \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        sT.xx()*sT.xx()*sT.xx(),                                            \
        .0,                                                                 \
        .0,                                                                 \
        sT.yy()*sT.yy()*sT.yy(),                                            \
        .0,                                                                 \
        sT.zz()*sT.zz()*sT.zz()                                             \
    );                                                                      \
}
transformFunc(gcbc)
#undef transformFunc

#define transformFunc(func)                                                 \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    const scalar sqrsTXX(sT.xx()*sT.xx());                                  \
    const scalar sqrsTYY(sT.yy()*sT.yy());                                  \
    const scalar sqrsTZZ(sT.zz()*sT.zz());                                  \
    return symmTensor                                                       \
    (                                                                       \
        sqrsTXX*sqrsTXX,                                                    \
        .0,                                                                 \
        .0,                                                                 \
        sqrsTYY*sqrsTYY,                                                    \
        .0,                                                                 \
        sqrsTZZ*sqrsTZZ                                                     \
    );                                                                      \
}
transformFunc(gqtc)
#undef transformFunc

#define transformFunc(func)                                                 \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        sT.xx()*sT.xx() - 1.,                                               \
        .0,                                                                 \
        .0,                                                                 \
        sT.yy()*sT.yy() - 1.,                                               \
        .0,                                                                 \
        sT.zz()*sT.zz() - 1.                                                \
    );                                                                      \
}
transformFunc(gsqrM1)
#undef transformFunc

#define transformFunc(func)                                                 \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        sT.xx()*sT.xx()*sT.xx() - 1.,                                       \
        .0,                                                                 \
        .0,                                                                 \
        sT.yy()*sT.yy()*sT.yy() - 1.,                                       \
        .0,                                                                 \
        sT.zz()*sT.zz()*sT.zz() - 1.                                        \
    );                                                                      \
}
transformFunc(gcbcM1)
#undef transformFunc

#define transformFunc(func)                                                 \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    const scalar sqrsTXX(sT.xx()*sT.xx());                                  \
    const scalar sqrsTYY(sT.yy()*sT.yy());                                  \
    const scalar sqrsTZZ(sT.zz()*sT.zz());                                  \
    return symmTensor                                                       \
    (                                                                       \
        sqrsTXX*sqrsTXX - 1.,                                               \
        .0,                                                                 \
        .0,                                                                 \
        sqrsTYY*sqrsTYY - 1.,                                               \
        .0,                                                                 \
        sqrsTZZ*sqrsTZZ - 1.                                                \
    );                                                                      \
}
transformFunc(gqtcM1)
#undef transformFunc

#define transformFunc(func)                                                 \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    const scalar sXX(sT.xx() + 1.);                                         \
    const scalar sYY(sT.yy() + 1.);                                         \
    const scalar sZZ(sT.zz() + 1.);                                         \
    return symmTensor                                                       \
    (                                                                       \
        sXX*sXX,                                                            \
        .0,                                                                 \
        .0,                                                                 \
        sYY*sYY,                                                            \
        .0,                                                                 \
        sZZ*sZZ                                                             \
    );                                                                      \
}
transformFunc(gsqrP1)
#undef transformFunc

#define transformFunc(func)                                                 \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    const scalar sXX(sT.xx() + 1.);                                         \
    const scalar sYY(sT.yy() + 1.);                                         \
    const scalar sZZ(sT.zz() + 1.);                                         \
    return symmTensor                                                       \
    (                                                                       \
        sXX*sXX*sXX,                                                        \
        .0,                                                                 \
        .0,                                                                 \
        sYY*sYY*sYY,                                                        \
        .0,                                                                 \
        sZZ*sZZ*sZZ                                                         \
    );                                                                      \
}
transformFunc(gcbcP1)
#undef transformFunc

#define transformFunc(func)                                                 \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    const scalar sXX(sT.xx() + 1.);                                         \
    const scalar sYY(sT.yy() + 1.);                                         \
    const scalar sZZ(sT.zz() + 1.);                                         \
    const scalar sqrsTXX(sXX*sXX);                                          \
    const scalar sqrsTYY(sYY*sYY);                                          \
    const scalar sqrsTZZ(sZZ*sZZ);                                          \
    return symmTensor                                                       \
    (                                                                       \
        sqrsTXX*sqrsTXX,                                                    \
        .0,                                                                 \
        .0,                                                                 \
        sqrsTYY*sqrsTYY,                                                    \
        .0,                                                                 \
        sqrsTZZ*sqrsTZZ                                                     \
    );                                                                      \
}
transformFunc(gqtcP1)
#undef transformFunc

#define transformFunc(func, stdfunc)                                        \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        ::stdfunc(sT.xx()),                                                 \
        .0,                                                                 \
        .0,                                                                 \
        ::stdfunc(sT.yy()),                                                 \
        .0,                                                                 \
        ::stdfunc(sT.zz())                                                  \
    );                                                                      \
}
transformFunc(glog, log)
transformFunc(gexp, exp)

transformFunc(gsqrt, sqrt)
transformFunc(gcbrt, cbrt)
#undef transformFunc

#define transformFunc(func, stdfunc)                                        \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        ::stdfunc(sT.xx()) - 1.,                                            \
        .0,                                                                 \
        .0,                                                                 \
        ::stdfunc(sT.yy()) - 1.,                                            \
        .0,                                                                 \
        ::stdfunc(sT.zz()) - 1.                                             \
    );                                                                      \
}
transformFunc(gsqrtM1, sqrt)
transformFunc(gcbrtM1, cbrt)
#undef transformFunc

#define transformFunc(func, stdfunc, rlogbase)                              \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        (::stdfunc(sT.xx())*mathConst::rlogbase),                           \
        .0,                                                                 \
        .0,                                                                 \
        (::stdfunc(sT.yy())*mathConst::rlogbase),                           \
        .0,                                                                 \
        (::stdfunc(sT.zz())*mathConst::rlogbase)                            \
    );                                                                      \
}
transformFunc(glog2, log, rlog2)
transformFunc(glog10, log, rlog10)
#undef transformFunc

#define transformFunc(func, stdfunc, base)                                  \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)(const symmTensor& sT)                     \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        (::stdfunc(base, sT.xx())),                                         \
        .0,                                                                 \
        .0,                                                                 \
        (::stdfunc(base, sT.yy())),                                         \
        .0,                                                                 \
        (::stdfunc(base, sT.zz()))                                          \
    );                                                                      \
}
transformFunc(gpow2, pow, 2.)
transformFunc(gpow10, pow, 10.)
#undef transformFunc

#define transformFunc(func, stdfunc, k)                                     \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func)                                           \
(                                                                           \
    const symmTensor& sT                                                    \
)                                                                           \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        (::stdfunc(sT.xx(), k)),                                            \
        .0,                                                                 \
        .0,                                                                 \
        (::stdfunc(sT.yy(), k)),                                            \
        .0,                                                                 \
        (::stdfunc(sT.zz(), k))                                             \
    );                                                                      \
}                                                                           \
                                                                            \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func##M1)                                       \
(                                                                           \
    const symmTensor& sT                                                    \
)                                                                           \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        ::stdfunc(sT.xx(), k) - 1.,                                         \
        .0,                                                                 \
        .0,                                                                 \
        ::stdfunc(sT.yy(), k) - 1.,                                         \
        .0,                                                                 \
        ::stdfunc(sT.zz(), k) - 1.                                          \
    );                                                                      \
}                                                                           \
                                                                            \
template<>                                                                  \
inline symmTensor GCRFUNCP2(func##P1)                                       \
(                                                                           \
    const symmTensor& sT                                                    \
)                                                                           \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        ::stdfunc(sT.xx() + 1., k),                                         \
        .0,                                                                 \
        .0,                                                                 \
        ::stdfunc(sT.yy() + 1., k),                                         \
        .0,                                                                 \
        ::stdfunc(sT.zz() + 1., k)                                          \
    );                                                                      \
}

transformFunc(gqtrt, pow, 0.25)
#undef transformFunc

#define transformFunc(func)                                                 \
template<>                                                                  \
inline symmTensor GCRFUNC2(func)                                            \
(                                                                           \
    const tensor& rT,                                                       \
    const symmTensor& sT                                                    \
)                                                                           \
{                                                                           \
    return                                                                  \
    symmTensor                                                              \
    (                                                                       \
        symm(transform(rT, GCRFUNCP2(func)(sT)))                            \
    );                                                                      \
}
transformFunc(glog)
transformFunc(gexp)
transformFunc(glog2)
transformFunc(glog10)
transformFunc(gpow2)
transformFunc(gpow10)

transformFunc(gsqr)
transformFunc(gsqrM1)
transformFunc(gsqrP1)

transformFunc(gcbc)
transformFunc(gcbcM1)
transformFunc(gcbcP1)

transformFunc(gqtc)
transformFunc(gqtcM1)
transformFunc(gqtcP1)

transformFunc(gsqrt)
transformFunc(gcbrt)
transformFunc(gqtrt)

transformFunc(gsqrtM1)
transformFunc(gcbrtM1)
transformFunc(gqtrtM1)
#undef transformFunc


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#define transformFunc(func, stdfunc)                                        \
template<>                                                                  \
inline symmTensor GCRFUNCP3(func)                                           \
(                                                                           \
    const symmTensor& sT,                                                   \
    const scalar& rlogbase                                                  \
)                                                                           \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        (::stdfunc(sT.xx())*rlogbase),                                      \
        .0,                                                                 \
        .0,                                                                 \
        (::stdfunc(sT.yy())*rlogbase),                                      \
        .0,                                                                 \
        (::stdfunc(sT.zz())*rlogbase)                                       \
    );                                                                      \
}

transformFunc(glogK, log)
#undef transformFunc

#define transformFunc(func, stdfunc)                                        \
template<>                                                                  \
inline symmTensor GCRFUNCP3(func)                                           \
(                                                                           \
    const symmTensor& sT,                                                   \
    const scalar& k                                                         \
)                                                                           \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        ::stdfunc(k, sT.xx()),                                              \
        .0,                                                                 \
        .0,                                                                 \
        ::stdfunc(k, sT.yy()),                                              \
        .0,                                                                 \
        ::stdfunc(k, sT.zz())                                               \
    );                                                                      \
}

transformFunc(gKpow, pow)
#undef transformFunc

#define transformFunc(func, stdfunc)                                        \
template<>                                                                  \
inline symmTensor GCRFUNCP3(func)                                           \
(                                                                           \
    const symmTensor& sT,                                                   \
    const scalar& k                                                         \
)                                                                           \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        ::stdfunc(sT.xx(), k),                                              \
        .0,                                                                 \
        .0,                                                                 \
        ::stdfunc(sT.yy(), k),                                              \
        .0,                                                                 \
        ::stdfunc(sT.zz(), k)                                               \
    );                                                                      \
}                                                                           \
                                                                            \
template<>                                                                  \
inline symmTensor GCRFUNCP3(func##M1)                                       \
(                                                                           \
    const symmTensor& sT,                                                   \
    const scalar& k                                                         \
)                                                                           \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        ::stdfunc(sT.xx(), k) - 1.,                                         \
        .0,                                                                 \
        .0,                                                                 \
        ::stdfunc(sT.yy(), k) - 1.,                                         \
        .0,                                                                 \
        ::stdfunc(sT.zz(), k) - 1.                                          \
    );                                                                      \
}                                                                           \
                                                                            \
template<>                                                                  \
inline symmTensor GCRFUNCP3(func##P1)                                       \
(                                                                           \
    const symmTensor& sT,                                                   \
    const scalar& k                                                         \
)                                                                           \
{                                                                           \
    return symmTensor                                                       \
    (                                                                       \
        ::stdfunc(sT.xx() + 1., k),                                         \
        .0,                                                                 \
        .0,                                                                 \
        ::stdfunc(sT.yy() + 1., k),                                         \
        .0,                                                                 \
        ::stdfunc(sT.zz() + 1., k)                                          \
    );                                                                      \
}

transformFunc(gpowK, pow)
#undef transformFunc

#define transformFunc(func)                                                 \
template<>                                                                  \
inline symmTensor GCRFUNC3(func)                                            \
(                                                                           \
    const tensor& rT,                                                       \
    const symmTensor& sT,                                                   \
    const scalar& base                                                      \
)                                                                           \
{                                                                           \
    return                                                                  \
    symmTensor                                                              \
    (                                                                       \
        symm(transform(rT, GCRFUNCP3(func)(sT, base)))                      \
    );                                                                      \
}
transformFunc(glogK)
transformFunc(gKpow)

transformFunc(gpowK)
transformFunc(gpowKM1)
transformFunc(gpowKP1)
#undef transformFunc


#undef GCRFUNCP3

#undef GCRFUNCP2

#undef GCRFUNC3

#undef GCRFUNC2


#undef COMMA

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
