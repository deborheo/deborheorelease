/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Niethammer <niethammer@gmx.de>

\*---------------------------------------------------------------------------*/

#include "DeferredCorrectionLimitedSchemeT.H"
#include "Gamma.H"
#include "limitedLinear.H"
#include "Minmod.H"
#include "MUSCL.H"
#include "OSPRE.H"
#include "SuperBee.H"
#include "UMIST.H"
#include "vanAlbada.H"
#include "vanLeer.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
    // Gamma
    makeDeferredTSurfaceInterpolationTypeScheme(GammaTDC,GammaLimiter,NVDTVD,magSqr,vector)
    makeDeferredTSurfaceInterpolationTypeScheme(GammaTDC,GammaLimiter,NVDTVD,magSqr,symmTensor)
    makeDeferredTSurfaceInterpolationTypeScheme(GammaTDC,GammaLimiter,NVDTVD,magSqr,tensor)

    // limitedLinear
    makeDeferredTSurfaceInterpolationTypeScheme(limitedLinearTDC,limitedLinearLimiter,NVDTVD,magSqr,vector)
    makeDeferredTSurfaceInterpolationTypeScheme(limitedLinearTDC,limitedLinearLimiter,NVDTVD,magSqr,symmTensor)
    makeDeferredTSurfaceInterpolationTypeScheme(limitedLinearTDC,limitedLinearLimiter,NVDTVD,magSqr,tensor)

    // Minmod
    makeDeferredTSurfaceInterpolationTypeScheme(MinmodTDC,MinmodLimiter,NVDTVD,magSqr,vector)
    makeDeferredTSurfaceInterpolationTypeScheme(MinmodTDC,MinmodLimiter,NVDTVD,magSqr,symmTensor)
    makeDeferredTSurfaceInterpolationTypeScheme(MinmodTDC,MinmodLimiter,NVDTVD,magSqr,tensor)

    // MUSCL
    makeDeferredTSurfaceInterpolationTypeScheme(MUSCLTDC,MUSCLLimiter,NVDTVD,magSqr,vector)
    makeDeferredTSurfaceInterpolationTypeScheme(MUSCLTDC,MUSCLLimiter,NVDTVD,magSqr,symmTensor)
    makeDeferredTSurfaceInterpolationTypeScheme(MUSCLTDC,MUSCLLimiter,NVDTVD,magSqr,tensor)

    // OSPRE
    makeDeferredTSurfaceInterpolationTypeScheme(OSPRETDC,OSPRELimiter,NVDTVD,magSqr,vector)
    makeDeferredTSurfaceInterpolationTypeScheme(OSPRETDC,OSPRELimiter,NVDTVD,magSqr,symmTensor)
    makeDeferredTSurfaceInterpolationTypeScheme(OSPRETDC,OSPRELimiter,NVDTVD,magSqr,tensor)

    // SuperBee
    makeDeferredTSurfaceInterpolationTypeScheme(SuperBeeTDC,SuperBeeLimiter,NVDTVD,magSqr,vector)
    makeDeferredTSurfaceInterpolationTypeScheme(SuperBeeTDC,SuperBeeLimiter,NVDTVD,magSqr,symmTensor)
    makeDeferredTSurfaceInterpolationTypeScheme(SuperBeeTDC,SuperBeeLimiter,NVDTVD,magSqr,tensor)

    // UMIST
    makeDeferredTSurfaceInterpolationTypeScheme(UMISTTDC,UMISTLimiter,NVDTVD,magSqr,vector)
    makeDeferredTSurfaceInterpolationTypeScheme(UMISTTDC,UMISTLimiter,NVDTVD,magSqr,symmTensor)
    makeDeferredTSurfaceInterpolationTypeScheme(UMISTTDC,UMISTLimiter,NVDTVD,magSqr,tensor)

    // vanAlbada
    makeDeferredTSurfaceInterpolationTypeScheme(vanAlbadaTDC,vanAlbadaLimiter,NVDTVD,magSqr,vector)
    makeDeferredTSurfaceInterpolationTypeScheme(vanAlbadaTDC,vanAlbadaLimiter,NVDTVD,magSqr,symmTensor)
    makeDeferredTSurfaceInterpolationTypeScheme(vanAlbadaTDC,vanAlbadaLimiter,NVDTVD,magSqr,tensor)

    // vanLeer
    makeDeferredTSurfaceInterpolationTypeScheme(vanLeerTDC,vanLeerLimiter,NVDTVD,magSqr,vector)
    makeDeferredTSurfaceInterpolationTypeScheme(vanLeerTDC,vanLeerLimiter,NVDTVD,magSqr,symmTensor)
    makeDeferredTSurfaceInterpolationTypeScheme(vanLeerTDC,vanLeerLimiter,NVDTVD,magSqr,tensor)

}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
