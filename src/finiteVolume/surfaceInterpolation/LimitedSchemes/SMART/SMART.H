/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::SMARTLimiter

SourceFiles
    SMART.C

Author
    Matthias Niethammer <niethammer@gmx.de>
 
Description
    Class with limiter function which returns the limiter for the
    SMART differencing scheme based on r obtained from the LimiterFunc
    class.

    This implementation of the SMART limiter is based on the reference:
    P.H. Gaskell, A.K.C. Lau, Curvature-compensated convective transport:
    SMART, a new boundedness-preserving transport algorithm, Int. J. Numer.
    Methods Fluids 8 (1988) 617.

    Used in conjunction with the template class LimitedScheme.

\*---------------------------------------------------------------------------*/

#ifndef SMART_H
#define SMART_H

#include "vector.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                        Class SMARTLimiter Declaration
\*---------------------------------------------------------------------------*/

template<class LimiterFunc>
class SMARTLimiter
:
    public LimiterFunc
{
    static const scalar k1_;
    static const scalar k2_;

public:

    SMARTLimiter(Istream& is)
    {}

    scalar limiter
    (
        const scalar cdWeight,
        const scalar faceFlux,
        const typename LimiterFunc::phiType& phiP,
        const typename LimiterFunc::phiType& phiN,
        const typename LimiterFunc::gradPhiType& gradcP,
        const typename LimiterFunc::gradPhiType& gradcN,
        const vector& d
    ) const
    {
        scalar r = LimiterFunc::r
        (
            faceFlux, phiP, phiN, gradcP, gradcN, d
        );

        // k1_ 0.75; k2_ 0.25;

        return max(min(min(2*r, k1_*r + k2_), 4), 0);
    }
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
