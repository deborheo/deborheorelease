/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Niethammer <niethammer@gmx.de>

\*---------------------------------------------------------------------------*/

#include "powerLaw.H"
#include "addToRunTimeSelectionTable.H"
#include "zeroGradientFvPatchField.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{

namespace fieldLambdaModels
{
    defineTypeNameAndDebug(powerLaw, 0);

    addToRunTimeSelectionTable(relaxationModel, powerLaw, dictionary);

    addToRunTimeSelectionTable(relaxationModel, powerLaw, dictionaryUPhi);

    addToRunTimeSelectionTable(relaxationModel, powerLaw, dictionaryUPhiN);
}

}

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

void Foam::fieldLambdaModels::powerLaw::calcLambda() const
{
    tmp<volScalarField> sr(relaxationModel::strainRate());

    scalarField& lambdaI = (*lambdaPtr_).primitiveFieldRef();
    const scalarField& srI = sr().primitiveField();

    lambdaI =
    (
        max
        (
            lambdaMin_.value(),
            min
            (
                lambdaMax_.value(),
                k_.value()*pow
                (
                    max(srI, scalar(SMALL)),
                    n_.value() - scalar(1)
                )
            )
        )
    )();

    forAll((*lambdaPtr_).boundaryField(), patchI)
    {
        if (!(*lambdaPtr_).boundaryField()[patchI].coupled())
        {
            scalarField& lambdaP = (*lambdaPtr_).boundaryFieldRef()[patchI];
            const scalarField& srP = sr().boundaryField()[patchI];

            lambdaP =
            (
                max
                (
                    lambdaMin_.value(),
                    min
                    (
                        lambdaMax_.value(),
                        k_.value()*pow
                        (
                            max(srP, scalar(SMALL)),
                            n_.value() - scalar(1)
                        )
                    )
                )
            )();
        }
    }

    (*lambdaPtr_).correctBoundaryConditions();
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::fieldLambdaModels::powerLaw::powerLaw
(
    const word& name,
    const dictionary& dict
)
:
    relaxationModel(name, dict),
    powerLawCoeffs_(dict.subDict(typeName + "Coeffs")),
    k_("k", dimTime, powerLawCoeffs_),
    n_("n", dimless, powerLawCoeffs_),
    lambdaMin_("lambdaMin", lambdaDimensions_, powerLawCoeffs_),
    lambdaMax_("lambdaMax", lambdaDimensions_, powerLawCoeffs_)
{}

Foam::fieldLambdaModels::powerLaw::powerLaw
(
    const word& name,
    const dictionary& dict,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    relaxationModel(name, dict, U, phi),
    powerLawCoeffs_(dict.subDict(typeName + "Coeffs")),
    k_("k", dimTime, powerLawCoeffs_),
    n_("n", dimless, powerLawCoeffs_),
    lambdaMin_("lambdaMin", lambdaDimensions_, powerLawCoeffs_),
    lambdaMax_("lambdaMax", lambdaDimensions_, powerLawCoeffs_)
{
    if
    (
        (!relaxationModel::checkDimensions(lambdaMin_))
      ||(!relaxationModel::checkDimensions(lambdaMax_))
    )
    {
        FatalErrorIn
        (
            "Foam::fieldLambdaModels::powerLaw::"
            "powerLaw(const word& name, const dictionary& dict, "
            "const volVectorField& U, const surfaceScalarField& phi)"
        )   << "    Incompatible dimensions. Aborting..."
            << abort(FatalError);
    }
}

Foam::fieldLambdaModels::powerLaw::powerLaw
(
    const word& name,
    const dictionary& dict,
    const volVectorField& U,
    const surfaceScalarField& phi,
    const word& lambdaName
)
:
    relaxationModel(name, dict, U, phi, lambdaName),
    powerLawCoeffs_(dict.subDict(typeName + "Coeffs")),
    k_("k", dimTime, powerLawCoeffs_),
    n_("n", dimless, powerLawCoeffs_),
    lambdaMin_("lambdaMin", lambdaDimensions_, powerLawCoeffs_),
    lambdaMax_("lambdaMax", lambdaDimensions_, powerLawCoeffs_)
{
    if
    (
        (!relaxationModel::checkDimensions(lambdaMin_))
      ||(!relaxationModel::checkDimensions(lambdaMax_))
    )
    {
        FatalErrorIn
        (
            "Foam::fieldLambdaModels::powerLaw::"
            "powerLaw(const word& name, const dictionary& dict, "
            "const volVectorField& U, const surfaceScalarField& phi)"
        )   << "    Incompatible dimensions. Aborting..."
            << abort(FatalError);
    }
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::fieldLambdaModels::powerLaw::~powerLaw()
{}

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void Foam::fieldLambdaModels::powerLaw::correct()
{
    makeLambdaPtr();

    calcLambda();
}

const Foam::relaxationModel::lambdaType& 
Foam::fieldLambdaModels::powerLaw::lambda() const
{
    if (relaxationModel::lambdaPtr_)
    {
        return (*relaxationModel::lambdaPtr_);
    }

    FatalErrorIn
    (
        "const typename Foam::relaxationModel"
        "::lambdaType& Foam::relaxationModel::powerLaw::lambda() "
        "const"
    )   << "    lambdaPtr_ is empty! Need to correct() relaxation time."
        << abort(FatalError);

    return (*relaxationModel::lambdaPtr_);
}

const Foam::relaxationModel::lambdaType& 
Foam::fieldLambdaModels::powerLaw::lambda(const bool& stabilize) const
{
    if(relaxationModel::lambdaPtr_)
    {
        // Stabilize for division
        if(stabilize)
        {
            stabilizeLambdaPtr();
        }
        return (*relaxationModel::lambdaPtr_);
    }

    FatalErrorIn
    (
        "const Foam::relaxationModel"
        "::lambdaType& Foam::fieldLambdaModels::powerLaw::lambda() "
        "const"
    )   << "    lambdaPtr_ is empty! Need to correct() lambda."
        << abort(FatalError);

    return (*relaxationModel::lambdaPtr_);
}

bool Foam::fieldLambdaModels::powerLaw::read
(
    const dictionary& relaxationProperties
)
{
    relaxationModel::read(relaxationProperties);

    powerLawCoeffs_ = relaxationProperties.subDict(typeName + "Coeffs");

    powerLawCoeffs_.readEntry("k", k_);
    powerLawCoeffs_.readEntry("n", n_);
    powerLawCoeffs_.readEntry("lambdaMin", lambdaMin_);
    powerLawCoeffs_.readEntry("lambdaMax", lambdaMax_);

    return true;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
