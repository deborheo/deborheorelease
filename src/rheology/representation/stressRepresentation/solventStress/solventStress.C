/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Niethammer <niethammer@gmx.de>

\*---------------------------------------------------------------------------*/

#include "solventStress.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(solventStress, 0);

    addToRunTimeSelectionTable
    (
        representation,
        solventStress,
        representation
    );

    addToRunTimeSelectionTable
    (
        representation,
        solventStress,
        representationAlpha
    );
}


// * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * * * //

// Divergence of the extra stress tensor divided by rho
template<class Type>
Foam::tmp<Foam::fvVectorMatrix>
inline Foam::solventStress::divTauT
(
    volVectorField& U, const Type& etaS
) const
{
    return tmp<fvVectorMatrix>
    (
        fvm::laplacian(etaS/rho(), U, "laplacian(etaS,U)")
    );
}// End divTau

// Divergence of the extra stress tensor divided by rho
template<class Type>
Foam::tmp<Foam::fvVectorMatrix>
inline Foam::solventStress::divTauT
(
    volVectorField& U
) const
{
    tmp<fvVectorMatrix> tdivTauT
    (
        fvm::laplacian(solventModel::etaS()/rho(), U, "laplacian(etaS,U)")
    );

    return tdivTauT;
}// End divTau

// Divergence of the extra stress tensor
template<class Type>
Foam::tmp<Foam::fvVectorMatrix>
inline Foam::solventStress::divTauT
(
    const surfaceScalarField& alpha1f, volVectorField& U
) const
{
    // Return a zero matrix
    tmp<fvVectorMatrix> tdivTauT
    (
        fvm::laplacian
        (
            dimensionedScalar
            (
                "0",
                solventModel::etaS().dimensions(),
                pTraits<scalar>::zero
            ), 
            U,
            "laplacian(etaS,U)"
        )
    );

    tdivTauT.ref().upper() = 0.;
    tdivTauT.ref().lower() = 0.;

    return tdivTauT;

}// End divTau


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::solventStress::solventStress
(
    const word& name,
    const dictionary& stab,
    const dictionary& rheology,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    genericRepresentation(name, U, phi),
    stressRepresentation(name, stab, rheology, U, phi),
    solventModel(rheology, U, phi),
    fullTypeName_(type())
{}

Foam::solventStress::solventStress
(
    const word& name,
    const dictionary& stab,
    const dictionary& rheology,
    const volScalarField& alpha1,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    genericRepresentation(name, U, phi),
    stressRepresentation(name, U, phi),
    solventModel(rheology, U, phi),
    fullTypeName_(type())
{}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

// Divergence of the extra stress tensor divided by rho
Foam::tmp<Foam::fvVectorMatrix>
Foam::solventStress::divTau(volVectorField& U)
{
    if (solventModel::solventActive())
    {
        solventModel::correct();

        return divTauT<volScalarField>(U, solventModel::etaS());
    }

    return divTauT<volScalarField>(U);
}

Foam::tmp<Foam::fvVectorMatrix>
Foam::solventStress::divTau
(
    const surfaceScalarField& alpha1f, volVectorField& U
)
{
    if (solventModel::solventActive())
    {
        solventModel::correct();
    }

    return divTauT<volScalarField>(alpha1f, U);
}

const Foam::solventStress::rGFType&
Foam::solventStress::tau() const
{
    if (tau_.size() > 0)
    {
        tmp<GeometricField<tensor, fvPatchField, volMesh> > L
        (
            fvc::grad(U(), "grad(U)")
        );

        tmp<GeometricField<symmTensor, fvPatchField, volMesh> > twoD
        (
            twoSymm(L())
        );

        L.clear();

        tau_ = solventModel::etaS()*twoD();

        return tau_;
    }
    else
    {
        FatalErrorIn
        (
            "const volSymmTensorField& tau() const "
        )   << "    tau_ is empty! Cannot access stress tensor data."
            << nl << "Aborting..." << nl << endl
            << abort(FatalError);
    }

    return tau_;
}

Foam::solventStress::rGFType&
Foam::solventStress::tauRef()
{
    if (tau_.size() > 0)
    {
        tmp<GeometricField<tensor, fvPatchField, volMesh> > L
        (
            fvc::grad(U(), "grad(U)")
        );

        const tmp<GeometricField<symmTensor, fvPatchField, volMesh> > twoD
        (
            twoSymm(L())
        );

        L.clear();

        tau_ = solventModel::etaS()*twoD();

        return tau_;
    }
    else
    {
        FatalErrorIn
        (
            "const volSymmTensorField& tau() const "
        )   << "    tau_ is empty! Cannot access stress tensor data."
            << nl << "Aborting..." << nl << endl
            << abort(FatalError);
    }

    return tau_;
}

// Correct the stresses (polymer part)
void Foam::solventStress::correct(const bool& lastIter)
{
    //if (singlePhase())
    {
        solventModel::correct();
        //correctThermo();
    }
}// End correct

const Foam::word& Foam::solventStress::fullTypeName() const
{
    return fullTypeName_;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
