/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Niethammer <niethammer@gmx.de>

\*---------------------------------------------------------------------------*/

#include "planarElongation.H"
#include "addToRunTimeSelectionTable.H"
//#include "fvc.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(planarElongation, 0);
    addToRunTimeSelectionTable(simpleFlowGenerator, planarElongation, word);
}

// * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * * * //

void Foam::planarElongation::checkLengthScale() const
{
    if ( !(lengthScale_.value() > VSMALL) )
    {
        FatalErrorInFunction
            << "lengthScale = " << lengthScale_.value()
            << " is not > 0 " << nl
            << exit(FatalError);
    }
}

void Foam::planarElongation::generateFlow() const
{
    generateBasicFields();

    // 1. Generate the velocity field
    volVectorField& U = URef();
    vectorField& UInt = U.primitiveFieldRef();

    const volVectorField& Cc = mesh().C();
    const surfaceVectorField& Cf = mesh().Cf();
    const vectorField& CcInt = Cc.primitiveField();

    scalar elongationRate = elongationRate_.value()/lengthScale_.value();

    forAll(UInt, cellI)
    {
        UInt[cellI] = vector
        (
            -elongationRate*CcInt[cellI].x(),
            0,
            elongationRate*CcInt[cellI].z()
        );
    }

    forAll(U.boundaryField(), patchI)
    {
        vectorField& UPatchI = U.boundaryFieldRef()[patchI];
        const vectorField& CfPatchI = Cf.boundaryField()[patchI];

        forAll(UPatchI, faceI)
        {
            UPatchI[faceI] = vector
            (
                -elongationRate*CfPatchI[faceI].x(),
                0,
                elongationRate*CfPatchI[faceI].z()
            );
        }
    }

    // 2. Compute the velocity gradient field
    // - analytically and/or
    // - numerically
    volTensorField& L = LRef();
    tensorField& LInt = L.primitiveFieldRef();

    forAll(LInt, cellI)
    {
        LInt[cellI] = tensor
        (
            -elongationRate, 0, 0,
            0, 0, 0,
            0, 0, elongationRate
        );
    }

    forAll(L.boundaryField(), patchI)
    {
        tensorField& LPatchI = L.boundaryFieldRef()[patchI];

        forAll(LPatchI, faceI)
        {
            LPatchI[faceI] = tensor
            (
                -elongationRate, 0, 0,
                0, 0, 0,
                0, 0, elongationRate
            );
        }
    }

//    volTensorField LNumerical = fvc::grad(U);
//    scalar error(0);
//    label count(0);

//    forAll(LNumerical, cellI)
//    {
//        error += sqr(mag(LNumerical[cellI] - L[cellI]))
//        count++;
//    }

//    error /= count;

    const volSymmTensorField& D = this->D();

    volScalarField& rate = rateRef();
    rate = Foam::sqrt(0.5*(D && D));
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::planarElongation::planarElongation
(
    const fvMesh& mesh
)
:
    simpleFlowGenerator(mesh),
    planarElongationCoeffs_
    (
        this->optionalSubDict("planarElongationCoeffs")
    ),
    elongationRate_("elongationRate", dimVelocity/dimLength, planarElongationCoeffs_),
    lengthScale_("lengthScale", dimLength, planarElongationCoeffs_)
{
    checkLengthScale();
}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::planarElongation::~planarElongation()
{
    simpleFlowGenerator::clearFlowData();
}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

bool Foam::planarElongation::read()
{
    if (simpleFlowGenerator::read())
    {
        planarElongationCoeffs_ =
            this->optionalSubDict("planarElongationCoeffs");

        planarElongationCoeffs_.readEntry("elongationRate", elongationRate_);
        planarElongationCoeffs_.readEntry("lengthScale", lengthScale_);

        return true;
    }

    return false;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
