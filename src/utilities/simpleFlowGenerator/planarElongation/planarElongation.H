/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::planarElongation

SourceFiles
    planarElongation.C

Author
    Matthias Niethammer <niethammer@gmx.de>

Description
    Generate a planar elongation flow field.

\*---------------------------------------------------------------------------*/

#ifndef planarElongation_H
#define planarElongation_H

#include "simpleFlowGenerator.H"
#include "dimensionedTypes.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                Class planarElongation Declaration
\*---------------------------------------------------------------------------*/

class planarElongation
:
    public simpleFlowGenerator
{

    // Private data

        dictionary planarElongationCoeffs_;

        dimensionedScalar elongationRate_;

        dimensionedScalar lengthScale_;


    // Private Member Functions

        //-  Check value of the length scale
        void checkLengthScale() const;

        //- No copy construct
        planarElongation(const planarElongation&) = delete;

        //- No copy assignment
        void operator=(const planarElongation&) = delete;

protected:

    // Protected Member Functions

        //- Generate the flow
        void generateFlow() const;


public:

    //- Runtime type information
    TypeName("planarElongation");


    // Constructors

        //- Construct from components
        planarElongation
        (
            const fvMesh& mesh
        );


    //- Destructor
    virtual ~planarElongation();


    // Member Functions

        //- Read the dictionary
        bool read();
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
