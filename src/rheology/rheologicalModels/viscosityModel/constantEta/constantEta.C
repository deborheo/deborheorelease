/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Niethammer <niethammer@gmx.de>

\*---------------------------------------------------------------------------*/

#include "constantEta.H"
#include "addToRunTimeSelectionTable.H"
#include "zeroGradientFvPatchField.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{

namespace ViscosityModels
{
    defineTypeNameAndDebug(constantEta, 0);

    addToRunTimeSelectionTable(ViscosityModel, constantEta, dictionary);

    addToRunTimeSelectionTable
    (
        ViscosityModel, constantEta, dictionaryUPhi
    );

    addToRunTimeSelectionTable
    (
        ViscosityModel, constantEta, dictionaryUPhiEta
    );
}

}

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

void Foam::ViscosityModels::constantEta::stabilizeEta()
{
    eta_.primitiveFieldRef()
        = (eta_.primitiveField() + SMALL);

    forAll(eta_.boundaryField(), patchI)
    {
        if (!eta_.boundaryField()[patchI].coupled())
        {
            eta_.boundaryFieldRef()[patchI]
            = (eta_.boundaryField()[patchI] + SMALL);
        }
    }

    eta_.correctBoundaryConditions();
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::ViscosityModels::constantEta::constantEta
(
    const word& name,
    const dictionary& dict
)
:
    ViscosityModel(name, dict),
    constantEtaCoeffs_(dict.subDict(typeName + "Coeffs")),
    eta0_("eta", etaDimensions_, constantEtaCoeffs_),
    eta_
    (
        IOobject
        (
            etaName_,
            ViscosityModel::U().time().timeName(),
            ViscosityModel::U().db(),
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        ViscosityModel::U().mesh(),
        eta0_,
        zeroGradientFvPatchField<scalar>::typeName
    )
{}

Foam::ViscosityModels::constantEta::constantEta
(
    const word& name,
    const dictionary& dict,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    ViscosityModel(name, dict, U, phi),
    constantEtaCoeffs_(dict.subDict(typeName + "Coeffs")),
    eta0_("eta", etaDimensions_, constantEtaCoeffs_),
    eta_
    (
        IOobject
        (
            etaName_,
            ViscosityModel::U().time().timeName(),
            ViscosityModel::U().db(),
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        ViscosityModel::U().mesh(),
        eta0_,
        zeroGradientFvPatchField<scalar>::typeName
    )
{
    if (!ViscosityModel::checkDimensions(eta0_))
    {
        FatalErrorIn
        (
            "Foam::ViscosityModels::constantEta::"
            "constantEta(const word& name, const dictionary& dict, "
            "const volVectorField& U, const surfaceScalarField& phi)"
        )   << "    " << eta0_.name() << " has incompatible dimensions!"
            << abort(FatalError);
    }
}

Foam::ViscosityModels::constantEta::constantEta
(
    const word& name,
    const dictionary& dict,
    const volVectorField& U,
    const surfaceScalarField& phi,
    const word& etaName
)
:
    ViscosityModel(name, dict, U, phi, etaName),
    constantEtaCoeffs_(dict.subDict(typeName + "Coeffs")),
    eta0_("eta", etaDimensions_, constantEtaCoeffs_),
    eta_
    (
        IOobject
        (
            etaName_,
            ViscosityModel::U().time().timeName(),
            ViscosityModel::U().db(),
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        ViscosityModel::U().mesh(),
        eta0_,
        zeroGradientFvPatchField<scalar>::typeName
    )
{
    if (!ViscosityModel::checkDimensions(eta0_))
    {
        FatalErrorIn
        (
            "Foam::ViscosityModels::constantEta::"
            "constantEta(const word& name, const dictionary& dict, "
            "const volVectorField& U, const surfaceScalarField& phi)"
        )   << "    " << eta0_.name() << " has incompatible dimensions!"
            << abort(FatalError);
    }
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::ViscosityModels::constantEta::~constantEta()
{}

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

const Foam::ViscosityModel::etaType& 
Foam::ViscosityModels::constantEta::eta(const bool& stabilize) const
{
    // Stabilize for division
    if (stabilize)
    {
        const_cast<Foam::ViscosityModels::constantEta*>(this)->stabilizeEta();
    }

    return eta_;
}

bool Foam::ViscosityModels::constantEta::read
(
    const dictionary& viscosityProperties
)
{
    ViscosityModel::read(viscosityProperties);

    constantEtaCoeffs_ = viscosityProperties.subDict(typeName + "Coeffs");

    dimensionedScalar tEta("eta", etaDimensions_, constantEtaCoeffs_);

    if
    (
        (tEta.value() > (eta0_.value()+VSMALL))
     || ((tEta.value()+VSMALL) < eta0_.value())
    )
    {
        //constantEtaCoeffs_.lookup("eta") >> eta0_;
        constantEtaCoeffs_.readEntry("eta", eta0_);

        Info << "Overwriting viscosity field " << eta_.name()
             << " with constant value " << eta0_.value() << endl;

        eta_ = eta0_;
    }

    return true;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
