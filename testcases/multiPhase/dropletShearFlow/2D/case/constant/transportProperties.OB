/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2012                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      transportProperties;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "../system/parametersDict"

phases (viscoelastic_liquid newtonian_liquid);

viscoelastic_liquid
{

    stabilization
    {
        // Diagonalization method
        diag            symmDiagQL;

        // Constitutive equation representation
        representation  rootConformationCorrectedSI;
        type            squareRoot;
        //k               4;

        // Fast back transformation
        diagonalization false;

        // Set alpha to 1 for both-sides-diffusion (BSD)
        alpha           0.0;
    }

    rheology
    {
        // Rheological model
        type           Oldroyd-B; 

        // Solvent viscosity model
        solventViscosity
        {
            viscosityModel      constantEta;

            constantEtaCoeffs
            {
                eta            eta [1 -1 -1 0 0 0 0] $etaS;
            }
        }

        // Polymer properties
        polymerProperties
        {
            viscosityModel      constantEta;

            constantEtaCoeffs
            {
                eta            eta [1 -1 -1 0 0 0 0] $etaP;
            }

            relaxationModel     constantLambda;

            constantLambdaCoeffs
            {
                lambda          lambda [0 0 1 0 0 0 0] $pLambda;
            }
        }

        //epsilon          epsilon [0 0 0 0 0 0 0] 0.25;

        // Thermo properties
        thermoProperties
        {
            thermoModel     isoThermal;
        }

        // Fluid density
        rho             rho [1 -3 0 0 0 0 0] @!rho1!@;

    }
}

newtonian_liquid
{
    rho             rho [1 -3 0 0 0 0 0] @!rho2!@;

    viscosityProperties
    {
        viscosityModel      constantEta;

        constantEtaCoeffs
        {
            eta            eta [1 -1 -1 0 0 0 0] @!eta2!@;
        }
    }
}


sigma               $sigma;
surfaceForces
{
    sigma	 $sigma;
    surfaceTensionForceModel @!curvature!@;//$STM;
    accelerationForceModel gravity;
    deltaFunctionModel alphaCSF;
}


// ************************************************************************* //
