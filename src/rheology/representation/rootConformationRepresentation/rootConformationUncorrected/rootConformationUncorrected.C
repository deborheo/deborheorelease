/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Niethammer <niethammer@gmx.de>

\*---------------------------------------------------------------------------*/

#include "rootConformationUncorrected.H"
#include "rootType.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(rootConformationUncorrected, 0);

    addToRunTimeSelectionTable
    (
        representation,
        rootConformationUncorrected,
        representation
    );

    addToRunTimeSelectionTable
    (
        representation,
        rootConformationUncorrected,
        representationE
    );

    addToRunTimeSelectionTable
    (
        representation,
        rootConformationUncorrected,
        representationAlpha
    );
}


// * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * * * //

// correctSinglePhase
void Foam::rootConformationUncorrected::correctSinglePhase(const bool& lastIter)
{
    // Transform stress tensor to root(C)
    A_ = GeometricField<symmTensor, fvPatchField, volMesh>
    (
        rootType_->transformStressToA(tau_, false, true)()
    );
    A_.correctBoundaryConditions();

    // Create velocity gradient
    tmp<GeometricField<tensor, fvPatchField, volMesh> > tL(createL());

    GeometricField<tensor, fvPatchField, volMesh>& L = tL.ref();

    // Decompose convected derivatives for root(C)-representation
    // and compute the r.h.s. expl. rheol. model terms
    tmp<GeometricField<symmTensor, fvPatchField, volMesh> > rhst
    (
        rootType_->decompose(L, true)
    );

    tL.clear();

    // Assemble AEqn (l.h.s.)
    tmp<fvMatrix<symmTensor> > AEqn
    (
        fvm::ddt(A_)
      + fvm::div(phi(), A_, "div(phi,tau)")
      ==
        rhst.cref()
    );

    rhst.clear();

    if(!lastIter)
    {
        AEqn.ref().relax();
    }
    else
    {
        if (U().mesh().relaxEquation(A_.name()+"Final"))
        {
            const scalar& alpha 
                = U().mesh().equationRelaxationFactor(A_.name()+"Final");

            AEqn.ref().relax(alpha);
        }
        else
        {
            AEqn.ref().relax(1);
        }
    }

    // Solve AEqn
    AEqn.ref().solve();

    AEqn.clear();

    // transform back
    tau_ = GeometricField<symmTensor, fvPatchField, volMesh>
    (
        rootType_->transformAToStress(A_, true, true)()
    );
    tau_.correctBoundaryConditions();
}// End correctSinglePhase

// Divergence of the extra stress tensor divided by rho
template<class Type>
Foam::tmp<Foam::fvVectorMatrix>
inline Foam::rootConformationUncorrected::divTauT
(
    volVectorField& U, const Type& etaS
) const
{
    if (alpha_ > SMALL)
    {
        // Standard BSD approach with face-interpolation correction
        return tmp<fvVectorMatrix>
        (
            fvc::div(tau_/rho(), "div(tau)")
          - fvc::laplacian(etaPEff_/rho(), U, "laplacian(etaPEff,U)")
          + fvm::laplacian((etaPEff_+etaS)/rho(), U, "laplacian(etaPEff+etaS,U)")
        );
    }

    // No BSD
    return tmp<fvVectorMatrix>
    (
        fvc::div(tau_/rho(), "div(tau)")
      + fvm::laplacian( etaS/rho(), U, "laplacian(etaS,U)")
    );
}// End divTau

// Divergence of the extra stress tensor divided by rho
template<class Type>
Foam::tmp<Foam::fvVectorMatrix>
inline Foam::rootConformationUncorrected::divTauT
(
    volVectorField& U
) const
{
    if (alpha_ > SMALL)
    {
        // Standard BSD approach with face-interpolation correction
        return tmp<fvVectorMatrix>
        (
            fvc::div(tau_/rho(), "div(tau)")
          - fvc::laplacian(etaPEff_/rho(), U, "laplacian(etaPEff,U)")
          + fvm::laplacian((etaPEff_)/rho(), U, "laplacian(etaPEff,U)")
        );
    }

    // No BSD
    tmp<fvVectorMatrix> tdivTauT
    (
        fvc::div(tau_/rho(), "div(tau)")
      + fvm::laplacian
        (
            dimensionedScalar
            (
                "0",
                etaPEff_.dimensions()/rho().dimensions(),
                pTraits<scalar>::zero
            ), 
            U,
            "laplacian(etaS,U)"
        )
    );

    tdivTauT.ref().upper() = 0.;
    tdivTauT.ref().lower() = 0.;

    return tdivTauT;
}// End divTau

// Divergence of the extra stress tensor
template<class Type>
Foam::tmp<Foam::fvVectorMatrix>
inline Foam::rootConformationUncorrected::divTauT
(
    const surfaceScalarField& alpha1f, volVectorField& U
) const
{
    const surfaceVectorField& Sf = tau_.mesh().Sf();
    const surfaceVectorField SfTauf = (Sf & fvc::interpolate(tau_, "interpolate(tau)").cref()).cref();
    if (alpha_ > SMALL)
    {
        // Standard BSD approach with face-interpolation correction
        return tmp<fvVectorMatrix>
        (
            fvc::div(alpha1f*SfTauf)
          - fvc::laplacian(etaPEff_, U, "laplacian(etaPEff,U)")
          + fvm::laplacian((etaPEff_), U, "laplacian(etaPEff,U)")
        );
    }

    // No BSD
    tmp<fvVectorMatrix> tdivTauT
    (
        fvc::div(alpha1f*SfTauf)
      + fvm::laplacian
        (
            dimensionedScalar
            (
                "0",
                etaPEff_.dimensions()/rho().dimensions(),
                pTraits<scalar>::zero
            ), 
            U,
            "laplacian(etaS,U)"
        )
    );

    tdivTauT.ref().upper() = 0.;
    tdivTauT.ref().lower() = 0.;

    return tdivTauT;
}// End divTau

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::rootConformationUncorrected::rootConformationUncorrected
(
    const word& name,
    const dictionary& stab,
    const dictionary& rheology,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    genericRepresentation(name, stab, rheology, U, phi),
    rootConformationRepresentation(name, stab, rheology, U, phi),
    rootType_
    (
        Foam::rootType::New
        (
            stab,
            *this
        )
    ),
    fullTypeName_(type() + "_" + rootType_->type())
{
    init();
}

Foam::rootConformationUncorrected::rootConformationUncorrected
(
    const word& name,
    const dictionary& stab,
    const dictionary& rheology,
    const volScalarField& alpha1,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    genericRepresentation(name, stab, rheology, alpha1, U, phi),
    rootConformationRepresentation(name, stab, rheology, alpha1, U, phi),
    rootType_
    (
        Foam::rootType::New
        (
            stab,
            *this
        )
    ),
    fullTypeName_(type() + "_" + rootType_->type())
{
    init();
}

Foam::rootConformationUncorrected::rootConformationUncorrected
(
    const word& name,
    const dictionary& stab,
    const dictionary& rheology,
    const volVectorField& U,
    const surfaceScalarField& phi,
    const volScalarField& etaS,
    const dimensionedScalar& rho
)
:
    genericRepresentation(name, stab, rheology, U, phi, etaS, rho),
    rootConformationRepresentation(name, stab, rheology, U, phi, etaS, rho),
    rootType_
    (
        Foam::rootType::New
        (
            stab,
            *this
        )
    ),
    fullTypeName_(type() + "_" + rootType_->type())
{
    init();
}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

// Divergence of the extra stress tensor divided by rho
Foam::tmp<Foam::fvVectorMatrix>
Foam::rootConformationUncorrected::divTau(volVectorField& U)
{
    if (singlePhase())
    {
        if (alpha_ > SMALL)
        {
            etaPEff_ = alpha_*law_->etaP();
        }

        if (law_->solventActive())
        {
            law_->correctSolvent();

            return divTauT<volScalarField>(U, etaS());
        }

        return divTauT<volScalarField>(U);
    }
    else
    {
        //return divTauT<volScalarField>(U, alpha1());
        if (alpha_ > SMALL)
        {
            etaPEff_ = alpha_*law_->etaP();
        }

        return divTauT<volScalarField>(U);
    }
}

// Divergence of the extra stress tensor
// used in incompressible VoF solvers to compute the face flux phi
Foam::tmp<Foam::fvVectorMatrix>
Foam::rootConformationUncorrected::divTau
(
    const surfaceScalarField& alpha1f, volVectorField& U
)
{
    if (alpha_ > SMALL)
    {
        etaPEff_ = alpha_*law_->etaP();
    }
    return divTauT<volScalarField>(alpha1f, U);
}

// correct the stresses (polymer part)
void Foam::rootConformationUncorrected::correct(const bool& lastIter)
{
    if(singlePhase())
    {
        law_->correctPolymer();
        this->correctSinglePhase(lastIter);
        law_->correctThermo();
    }
    else
    {
//        law_->correctPolymer();
//        this->correctTwoPhase(lastIter);
//        law_->correctThermo();
    }
}// End correct

bool Foam::rootConformationUncorrected::init() const
{
    if (currPattern_ > STANDARD)
    {
        return rootType_->makeCPtr(A_, true, true);
    }
    return false;
}

const Foam::word& Foam::rootConformationUncorrected::fullTypeName() const
{
    return fullTypeName_;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
