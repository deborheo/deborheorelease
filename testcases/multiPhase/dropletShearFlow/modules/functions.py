import os
import matplotlib.pyplot as plt
import pandas as pd
import re
from matplotlib import rcParams
import numpy as np
import math


class Funcs:
   # Custom sorting function to convert scientific notation to numbers and sort
    @classmethod
    def custom_sort(self,folder_names):
        def convert_to_number(name):
            if 'e' in name:
                base, exponent = name.split('e')
                return float(base) * 10**int(exponent)
            else:
                return float(name)

        return sorted(folder_names, key=convert_to_number)

    # Calculate the geometric center of the point cloud
    @classmethod
    def calculate_center(self,points):
        if len(points) == 0:
            return None

        center_i = sum(points) / len(points)
        return center_i

    # Interface distance from the geometric drop center to the interface point
    @classmethod
    def calc_interface_distance(self,centre, x, y, z):
        dist = self.mag([x - centre[0], y - centre[1], z - centre[2]])
        return dist
    
    # Function definition to compute magnitude of the vector
    @classmethod
    def mag(self,vector):
        return math.sqrt(sum(pow(element, 2) for element in vector))

    # Function to calculate the deformation parameter
    @classmethod
    def calcD(self,folder_name, time_folder,interface_distance_max,interface_distance_min,deformation_parameter,if_coord_array):
        vtk_file_path = os.path.join(folder_name, 'postProcessing', 'surfaces', str(time_folder), 'isoAlpha.vtk')

        x_component, y_component, z_component = [], [], []

        interface_distance = []
        interface_distance_max = 0
        interface_distance_min = 0
        with open(vtk_file_path, 'r') as vtk_file:
            lines = vtk_file.readlines()

            # Find the line containing "POINTS" and extract the number of points.
            for line in lines:
                if "POINTS" in line:
                    break_up = line.split()
                    number_of_coordinates = int(break_up[1])
                    if_coord_array = 1

                if "\n" not in line[0] and if_coord_array == 1 and "POINTS" not in line:
                    coordBreakUp = line.split()

                    # length is hard coded because the format of the .vtk file
                    # some lines may have three data points, some have 2 or 1.
                    if((len(coordBreakUp)>0)):
                        x_component.append(float (coordBreakUp[0]))
                        y_component.append(float (coordBreakUp[1]))
                        z_component.append(0.0)

                    if((len(coordBreakUp)>3)):
                        x_component.append(float (coordBreakUp[3]))
                        y_component.append(float (coordBreakUp[4]))
                        z_component.append(0.0)

                    if((len(coordBreakUp)>6) and (len(coordBreakUp)<10)):
                        x_component.append(float (coordBreakUp[6]))
                        y_component.append(float (coordBreakUp[7]))
                        z_component.append(0.0)

                if "\n" in line[0] and if_coord_array == 1:
                    if_coord_array = 2

        # Volumetric centre of the point clouds read from the VTK files.
        # to calculate the maximum and minumum interface distance
        centre = [
            Funcs.calculate_center(x_component),
            Funcs.calculate_center(y_component),
            Funcs.calculate_center(z_component)
        ]

        interface_distance = [Funcs.calc_interface_distance(centre, x_component[i], y_component[i], z_component[i])
                              for i in range(len(x_component))]

        interface_distance_max = max(interface_distance)  # Rmax
        interface_distance_min = min(interface_distance)  # Rmin

        deformation_parameter.append(
            (interface_distance_max - interface_distance_min) / (interface_distance_max + interface_distance_min))
        return deformation_parameter

    # Function to calculate the deformation parameter
    @classmethod
    def calcAngle(self,folder_name, time_folder,interface_distance_max,interface_distance_min,deformationAngle,if_coord_array,central_axis):
        vtk_file_path = os.path.join(folder_name, 'postProcessing', 'surfaces', str(time_folder), 'isoAlpha.vtk')

        x_component, y_component, z_component = [], [], []
        interfaceDistance = []
        interfaceDistanceMax = 0
        interfaceDistanceMin = 0
        
        with open(vtk_file_path, 'r') as vtk_file:
            lines = vtk_file.readlines()

    #       Find the line containing "POINTS" and extract the number of points.
            for line in lines:          
                if "POINTS" in line:
                    breakUp = line.split()
                    numberOfCoordinates = int(breakUp[1])
                    if_coord_array = 1;   

                if "\n" not in line[0] and if_coord_array ==1 and "POINTS" not in line:
                    coordBreakUp = line.split()

                    if((len(coordBreakUp)>0)):
                        x_component.append(float (coordBreakUp[0]))
                        y_component.append(float (coordBreakUp[1]))
                        z_component.append(0.0)

                    if((len(coordBreakUp)>3)):
                        x_component.append(float (coordBreakUp[3]))
                        y_component.append(float (coordBreakUp[4]))
                        z_component.append(0.0)

                    if((len(coordBreakUp)>6) and (len(coordBreakUp)<10)):
                        x_component.append(float (coordBreakUp[6]))
                        y_component.append(float (coordBreakUp[7]))
                        z_component.append(0.0)


                if "\n" in line[0] and if_coord_array ==1:
                    if_coord_array =2

        #Volumetric centre of the point clouds read from the VTK files.
        # centre = Sum_x / nX; 
        centre = [Funcs.calculate_center(x_component),
                  Funcs.calculate_center(y_component),
                  Funcs.calculate_center(z_component)]

        interfaceDistance = [Funcs.calc_interface_distance(centre, x_component[i],y_component[i],z_component[i]) 
                                                   for i in range (len(x_component))]

        interfaceDistanceMax = max (interfaceDistance) #Rmax
        interfaceDistanceMin = min (interfaceDistance) #Rmin
        
        maxIndex = interfaceDistance.index(interfaceDistanceMax)
        
        #vector from drop centre to maxPoint on the interface  (maxPoint - centre)
        v1 = [x_component[maxIndex] - centre[0], 
                    y_component[maxIndex] - centre[1],
                    z_component[maxIndex] - centre[2]]
        
        #deformation angle
        theta = math.acos ((np.dot(central_axis, v1)) / (Funcs.mag(central_axis)*Funcs.mag(v1)))*180.0 / math.pi
        
        if theta > 90.0:
            theta = 180.0 - theta
        
        deformationAngle.append(theta)
        return deformationAngle
    
    # Function to calculate the deformation parameteri 3D
    @classmethod
    def calc3D(self,folder_name, time_folder,interface_distance_max,interface_distance_min,deformation_parameter,if_coord_array):
        vtk_file_path = os.path.join(folder_name, 'postProcessing', 'surfaces', str(time_folder), 'isoAlpha.vtk')

        x_component, y_component, z_component = [], [], []

        interface_distance = []
        interface_distance_max = 0
        interface_distance_min = 0
        with open(vtk_file_path, 'r') as vtk_file:
            lines = vtk_file.readlines()

            # Find the line containing "POINTS" and extract the number of points.
            for line in lines:
                if "POINTS" in line:
                    break_up = line.split()
                    number_of_coordinates = int(break_up[1])
                    if_coord_array = 1

                if "\n" not in line[0] and if_coord_array == 1 and "POINTS" not in line:
                    coordBreakUp = line.split()

                    # length is hard coded because the format of the .vtk file
                    # some lines may have three data points, some have 2 or 1.
                    if((len(coordBreakUp)>0)):
                        x_component.append(float (coordBreakUp[0]))
                        y_component.append(float (coordBreakUp[1]))
                        z_component.append(float (coordBreakUp[2]))

                    if((len(coordBreakUp)>3)):
                        x_component.append(float (coordBreakUp[3]))
                        y_component.append(float (coordBreakUp[4]))
                        z_component.append(float (coordBreakUp[5]))

                    if((len(coordBreakUp)>6) and (len(coordBreakUp)<10)):
                        x_component.append(float (coordBreakUp[6]))
                        y_component.append(float (coordBreakUp[7]))
                        z_component.append(float (coordBreakUp[8]))

                if "\n" in line[0] and if_coord_array == 1:
                    if_coord_array = 2

        # Volumetric centre of the point clouds read from the VTK files.
        # to calculate the maximum and minumum interface distance
        centre = [
            Funcs.calculate_center(x_component),
            Funcs.calculate_center(y_component),
            Funcs.calculate_center(z_component)
        ]

        interface_distance = [Funcs.calc_interface_distance(centre, x_component[i], y_component[i], z_component[i])
                              for i in range(len(x_component))]

        interface_distance_max = max(interface_distance)  # Rmax
        interface_distance_min = min(interface_distance)  # Rmin

        deformation_parameter.append(
            (interface_distance_max - interface_distance_min) / (interface_distance_max + interface_distance_min))
        return deformation_parameter

    # Function to calculate the deformation parameter
    @classmethod
    def calc3Angle(self,folder_name, time_folder,interface_distance_max,interface_distance_min,deformationAngle,if_coord_array,central_axis):
        vtk_file_path = os.path.join(folder_name, 'postProcessing', 'surfaces', str(time_folder), 'isoAlpha.vtk')

        x_component, y_component, z_component = [], [], []
        interfaceDistance = []
        interfaceDistanceMax = 0
        interfaceDistanceMin = 0
        
        with open(vtk_file_path, 'r') as vtk_file:
            lines = vtk_file.readlines()

    #       Find the line containing "POINTS" and extract the number of points.
            for line in lines:          
                if "POINTS" in line:
                    breakUp = line.split()
                    numberOfCoordinates = int(breakUp[1])
                    if_coord_array = 1;   

                if "\n" not in line[0] and if_coord_array ==1 and "POINTS" not in line:
                    coordBreakUp = line.split()

                    if((len(coordBreakUp)>0)):
                        x_component.append(float (coordBreakUp[0]))
                        y_component.append(float (coordBreakUp[1]))
                        z_component.append(float (coordBreakUp[2]))

                    if((len(coordBreakUp)>3)):
                        x_component.append(float (coordBreakUp[3]))
                        y_component.append(float (coordBreakUp[4]))
                        z_component.append(float (coordBreakUp[5]))

                    if((len(coordBreakUp)>6) and (len(coordBreakUp)<10)):
                        x_component.append(float (coordBreakUp[6]))
                        y_component.append(float (coordBreakUp[7]))
                        z_component.append(float (coordBreakUp[8]))


                if "\n" in line[0] and if_coord_array ==1:
                    if_coord_array =2

        #Volumetric centre of the point clouds read from the VTK files.
        # centre = Sum_x / nX; 
        centre = [Funcs.calculate_center(x_component),
                  Funcs.calculate_center(y_component),
                  Funcs.calculate_center(z_component)]

        interfaceDistance = [Funcs.calc_interface_distance(centre, x_component[i],y_component[i],z_component[i]) 
                                                   for i in range (len(x_component))]

        interfaceDistanceMax = max (interfaceDistance) #Rmax
        interfaceDistanceMin = min (interfaceDistance) #Rmin
        
        maxIndex = interfaceDistance.index(interfaceDistanceMax)
        
        #vector from drop centre to maxPoint on the interface  (maxPoint - centre)
        v1 = [x_component[maxIndex] - centre[0], 
                    y_component[maxIndex] - centre[1],
                    z_component[maxIndex] - centre[2]]
        
        #deformation angle
        theta = math.acos ((np.dot(central_axis, v1)) / (Funcs.mag(central_axis)*Funcs.mag(v1)))*180.0 / math.pi
        
        if theta > 90.0:
            theta = 180.0 - theta
        
        deformationAngle.append(theta)
        return deformationAngle
          
