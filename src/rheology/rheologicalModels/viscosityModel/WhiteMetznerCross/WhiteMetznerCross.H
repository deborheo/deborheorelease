/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Class
    WhiteMetznerCross

SourceFiles
    WhiteMetznerCross.C

Author
    Matthias Niethammer <niethammer@gmx.de>

Description
    Power-law/Cross viscosity model for the White-Metzner stress equation

\*---------------------------------------------------------------------------*/

#ifndef WhiteMetznerCross_H
#define WhiteMetznerCross_H

#include "ViscosityModel.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

namespace ViscosityModels
{

/*---------------------------------------------------------------------------*\
                        Class WhiteMetznerCross Declaration
\*---------------------------------------------------------------------------*/

class WhiteMetznerCross
:
    public ViscosityModel
{

private:

    // Private data

        dictionary WhiteMetznerCrossCoeffs_;

        dimensionedScalar k_;
        dimensionedScalar n_;
        dimensionedScalar eta0_;


    // Private Member Functions

        //- Calculate the viscosity
        void calcEta() const;

        //- Disallow copy construct
        WhiteMetznerCross(const WhiteMetznerCross&);

        //- Disallow default bitwise assignment
        void operator=(const WhiteMetznerCross&);


public:

    //- Runtime type information
    TypeName("WhiteMetznerCross");


    // Constructor

        //- Construct from components
        WhiteMetznerCross
        (
            const word& name,
            const dictionary& dict
        );

        //- Construct from components
        WhiteMetznerCross
        (
            const word& name,
            const dictionary& dict,
            const volVectorField& U,
            const surfaceScalarField& phi
        );

        //- Construct from components
        WhiteMetznerCross
        (
            const word& name,
            const dictionary& dict,
            const volVectorField& U,
            const surfaceScalarField& phi,
            const word& etaName
        );


    // Destructor
    virtual ~WhiteMetznerCross();


    // Member Functions

        //- Correct the viscosity
        virtual void correct();

        //- Return the viscosity
        virtual const ViscosityModel::etaType& eta() const;

        //- Return the viscosity & stabilize for division
        virtual const ViscosityModel::etaType& eta(const bool& stabilize) const;

        //- Read viscosityProperties dictionary
        virtual bool read(const dictionary& viscosityProperties);

};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace ViscosityModels

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
