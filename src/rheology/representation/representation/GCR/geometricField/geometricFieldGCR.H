/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

InNamespace
    Foam::GCR

SourceFiles
    geometricFieldGCR.C

Author
    Matthias Niethammer <niethammer@gmx.de>

Description
    GCR geometricField transformation templates

    You may refer to this software as :
        Matthias Niethammer, Holger Marschall, Christian Kunkelmann,
        and Dieter Bothe. A numerical stabilization framework for viscoelastic
        fluid flow using the finite volume method on general unstructured 
        meshes. Int. J. Numer. Methods Fluids 86, 2 (2018), 131-166

\*---------------------------------------------------------------------------*/

#ifndef geometricFieldGCR_H
#define geometricFieldGCR_H

#include "fieldFieldGCR.H"
#include "GeometricFields.H"
#include "calculatedFvPatchField.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace GCR
{

#define COMMA ,

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#define TEMPLATE                                                              \
template                                                                      \
<                                                                             \
    class ReturnType, class Type1, class Type2,                               \
    template<class> class PatchField, class GeoMesh                           \
>

#define FIELDFUNC(Func)                                                       \
::Foam::GCR::Func<ReturnType COMMA Type1 COMMA Type2>

#define FIELDFIELDFUNC(Func)                                                  \
::Foam::GCR::Func<PatchField COMMA ReturnType COMMA Type1 COMMA Type2>

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#define BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, Func)                   \
                                                                              \
TEMPLATE                                                                      \
void Func                                                                     \
(                                                                             \
    GeometricField<ReturnType, PatchField, GeoMesh>& res,                     \
    const GeometricField<Type1, PatchField, GeoMesh>& gf1,                    \
    const GeometricField<Type2, PatchField, GeoMesh>& gf2                     \
)                                                                             \
{                                                                             \
    FIELDFUNC(Func)                                                           \
    (                                                                         \
        res.primitiveFieldRef(), gf1.primitiveField(), gf2.primitiveField()   \
    );                                                                        \
                                                                              \
    FIELDFIELDFUNC(Func)                                                      \
    (                                                                         \
      res.boundaryFieldRef(), gf1.boundaryField(), gf2.boundaryField()        \
    );                                                                        \
}                                                                             \
                                                                              \
TEMPLATE                                                                      \
tmp<GeometricField<ReturnType, PatchField, GeoMesh> > Func                    \
(                                                                             \
    const GeometricField<Type1, PatchField, GeoMesh>& gf1,                    \
    const GeometricField<Type2, PatchField, GeoMesh>& gf2                     \
)                                                                             \
{                                                                             \
    tmp<GeometricField<ReturnType, PatchField, GeoMesh> > tRes                \
    (                                                                         \
        new GeometricField<ReturnType, PatchField, GeoMesh>                   \
        (                                                                     \
            IOobject                                                          \
            (                                                                 \
                #Func "Transform(" + gf1.name() + ',' + gf2.name() + ')',     \
                gf2.instance(),                                               \
                gf2.db(),                                                     \
                IOobject::NO_READ,                                            \
                IOobject::NO_WRITE                                            \
            ),                                                                \
            gf2.mesh(),                                                       \
            gf2.dimensions()                                                  \
        )                                                                     \
    );                                                                        \
                                                                              \
    Foam::GCR::Func(tRes.ref(), gf1, gf2);                                    \
                                                                              \
    tRes.ref().correctBoundaryConditions();                                   \
                                                                              \
    return tRes;                                                              \
}                                                                             \
                                                                              \
TEMPLATE                                                                      \
tmp<GeometricField<ReturnType, PatchField, GeoMesh> > Func                    \
(                                                                             \
    const GeometricField<Type1, PatchField, GeoMesh>& gf1,                    \
    const tmp<GeometricField<Type2, PatchField, GeoMesh> >& tgf2              \
)                                                                             \
{                                                                             \
    const GeometricField<Type2, PatchField, GeoMesh>& gf2 = tgf2.cref();      \
                                                                              \
    tmp<GeometricField<ReturnType, PatchField, GeoMesh> > tRes                \
    (                                                                         \
        reuseTmpGeometricField<ReturnType, Type2, PatchField, GeoMesh>::New   \
        (                                                                     \
            tgf2,                                                             \
            #Func "Transform(" + gf1.name() + ',' + gf2.name() + ')',         \
            gf2.dimensions()                                                  \
        )                                                                     \
    );                                                                        \
                                                                              \
    Func(tRes.ref(), gf1, gf2);                                               \
                                                                              \
    reuseTmpGeometricField                                                    \
        <ReturnType, Type2, PatchField, GeoMesh>::clear(tgf2);                \
                                                                              \
    tRes.ref().correctBoundaryConditions();                                   \
                                                                              \
    return tRes;                                                              \
}                                                                             \
                                                                              \
TEMPLATE                                                                      \
tmp<GeometricField<ReturnType, PatchField, GeoMesh> > Func                    \
(                                                                             \
    const tmp<GeometricField<Type1, PatchField, GeoMesh> >& tgf1,             \
    const GeometricField<Type2, PatchField, GeoMesh>& gf2                     \
)                                                                             \
{                                                                             \
    const GeometricField<Type1, PatchField, GeoMesh>& gf1 = tgf1.cref();      \
                                                                              \
    tmp<GeometricField<ReturnType, PatchField, GeoMesh> > tRes                \
    (                                                                         \
        reuseTmpGeometricField<ReturnType, Type1, PatchField, GeoMesh>::New   \
        (                                                                     \
            tgf1,                                                             \
            #Func "Transform(" + gf1.name() + ',' + gf2.name() + ')',         \
            gf2.dimensions()                                                  \
        )                                                                     \
    );                                                                        \
                                                                              \
    Func(tRes.ref(), gf1, gf2);                                               \
                                                                              \
    reuseTmpGeometricField                                                    \
        <ReturnType, Type1, PatchField, GeoMesh>::clear(tgf1);                \
                                                                              \
    tRes.ref().correctBoundaryConditions();                                   \
                                                                              \
    return tRes;                                                              \
}                                                                             \
                                                                              \
TEMPLATE                                                                      \
tmp<GeometricField<ReturnType, PatchField, GeoMesh> > Func                    \
(                                                                             \
    const tmp<GeometricField<Type1, PatchField, GeoMesh> >& tgf1,             \
    const tmp<GeometricField<Type2, PatchField, GeoMesh> >& tgf2              \
)                                                                             \
{                                                                             \
    const GeometricField<Type1, PatchField, GeoMesh>& gf1 = tgf1.cref();      \
    const GeometricField<Type2, PatchField, GeoMesh>& gf2 = tgf2.cref();      \
                                                                              \
    tmp<GeometricField<ReturnType, PatchField, GeoMesh> > tRes                \
    (                                                                         \
        reuseTmpTmpGeometricField                                             \
            <ReturnType, Type1, Type1, Type2, PatchField, GeoMesh>            \
        ::New                                                                 \
        (                                                                     \
            tgf1,                                                             \
            tgf2,                                                             \
            #Func "Transform(" + gf1.name() + ',' + gf2.name() + ')',         \
            gf2.dimensions()                                                  \
        )                                                                     \
    );                                                                        \
                                                                              \
    Func(tRes.ref(), gf1, gf2);                                               \
                                                                              \
    reuseTmpTmpGeometricField                                                 \
        <ReturnType, Type1, Type1, Type2, PatchField, GeoMesh>                \
        ::clear(tgf1, tgf2);                                                  \
                                                                              \
    tRes.ref().correctBoundaryConditions();                                   \
                                                                              \
    return tRes;                                                              \
}
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, glog)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gexp)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, glog2)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, glog10)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gpow2)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gpow10)

BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gsqr)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gsqrM1)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gsqrP1)

BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gcbc)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gcbcM1)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gcbcP1)

BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gqtc)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gqtcM1)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gqtcP1)

BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gsqrt)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gcbrt)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gqtrt)

BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gsqrtM1)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gcbrtM1)
BINARY_FUNCTION_GCR(ReturnType, Type1, Type2, gqtrtM1)
#undef BINARY_FUNCTION_GCR

#undef FIELDFIELDFUNC

#undef FIELDFUNC

#undef TEMPLATE

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#define TEMPLATE                                                              \
template                                                                      \
<                                                                             \
    class ReturnType, class Type1, class Type2,                               \
    template<class> class PatchField, class GeoMesh                           \
>

#define FIELDFUNC(Func)                                                       \
::Foam::GCR::Func<ReturnType COMMA Type1 COMMA Type2>

#define FIELDFIELDFUNC(Func)                                                  \
::Foam::GCR::Func<PatchField COMMA ReturnType COMMA Type1 COMMA Type2>

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#define TERNARY_FUNCTION_GCR_FFS(ReturnType, Type1, Type2, Func)              \
                                                                              \
TEMPLATE                                                                      \
void Func                                                                     \
(                                                                             \
    GeometricField<ReturnType, PatchField, GeoMesh>& res,                     \
    const GeometricField<Type1, PatchField, GeoMesh>& gf1,                    \
    const GeometricField<Type2, PatchField, GeoMesh>& gf2,                    \
    const Foam::scalar& s                                                     \
)                                                                             \
{                                                                             \
    FIELDFUNC(Func)                                                           \
    (                                                                         \
        res.primitiveFieldRef(), gf1.primitiveField(), gf2.primitiveField(), s \
    );                                                                        \
                                                                              \
    FIELDFIELDFUNC(Func)                                                      \
    (                                                                         \
      res.boundaryFieldRef(), gf1.boundaryField(), gf2.boundaryField(), s     \
    );                                                                        \
}                                                                             \
                                                                              \
TEMPLATE                                                                      \
tmp<GeometricField<ReturnType, PatchField, GeoMesh> > Func                    \
(                                                                             \
    const GeometricField<Type1, PatchField, GeoMesh>& gf1,                    \
    const GeometricField<Type2, PatchField, GeoMesh>& gf2,                    \
    const Foam::scalar& s                                                     \
)                                                                             \
{                                                                             \
    tmp<GeometricField<ReturnType, PatchField, GeoMesh> > tRes                \
    (                                                                         \
        new GeometricField<ReturnType, PatchField, GeoMesh>                   \
        (                                                                     \
            IOobject                                                          \
            (                                                                 \
                #Func "Transform(" + gf1.name() + ',' + gf2.name() + ')',     \
                gf2.instance(),                                               \
                gf2.db(),                                                     \
                IOobject::NO_READ,                                            \
                IOobject::NO_WRITE                                            \
            ),                                                                \
            gf2.mesh(),                                                       \
            gf2.dimensions()                                                  \
        )                                                                     \
    );                                                                        \
                                                                              \
    Foam::GCR::Func(tRes.ref(), gf1, gf2, s);                                 \
                                                                              \
    tRes.ref().correctBoundaryConditions();                                   \
                                                                              \
    return tRes;                                                              \
}

TERNARY_FUNCTION_GCR_FFS(ReturnType, Type1, Type2, glogK)
TERNARY_FUNCTION_GCR_FFS(ReturnType, Type1, Type2, gKpow)

TERNARY_FUNCTION_GCR_FFS(ReturnType, Type1, Type2, gpowK)
TERNARY_FUNCTION_GCR_FFS(ReturnType, Type1, Type2, gpowKM1)
TERNARY_FUNCTION_GCR_FFS(ReturnType, Type1, Type2, gpowKP1)

#undef TERNARY_FUNCTION_GCR_FFS

#undef FIELDFIELDFUNC

#undef FIELDFUNC

#undef TEMPLATE

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#undef COMMA

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace GCR

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
