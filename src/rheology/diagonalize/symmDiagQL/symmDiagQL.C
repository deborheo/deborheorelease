/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Niethammer <niethammer@gmx.de>

\*---------------------------------------------------------------------------*/

#include "symmDiagQL.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(symmDiagQL, 0);
    addToRunTimeSelectionTable(diagonalize, symmDiagQL, dictionary);
}


const Foam::scalar Foam::symmDiagQL::small_ = Foam::scalar(1e-60);

// * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::symmDiagQL::symmDiagQL
(
    const word& name,
    const dictionary& dict
)
:
    diagonalize(name, dict),
    eigenValuesSTPtr_(NULL),
    eigenVectorsTPtr_(NULL)
{}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

// Eigenvalues tensorField
Foam::GeometricField<Foam::symmTensor, Foam::fvPatchField, Foam::volMesh>
Foam::symmDiagQL::eigenValuesST
(
    const Foam::GeometricField
    <
        Foam::symmTensor, Foam::fvPatchField, Foam::volMesh
    >& A,
    const bool& solve
) const
{
    if (solve)
    {
        // diagonalize A
        symmDiagQL::diag(A, false);
    }
    else if (!eigenValuesSTPtr_)
    {
        WarningIn("Foam::symmDiagQL::eigenValuesST(")
                << "const Foam::GeometricField<"
                << "Foam::symmTensor, Foam::fvPatchField, Foam::volMesh"
                << "& A, const bool& solve) " << endl
                << "No *eigenValuesSTPtr_ found " << endl
                << "Starting diagonalization... " << endl;

        // diagonalize A
        symmDiagQL::diag(A, false);
    }

    return GeometricField<symmTensor, fvPatchField, volMesh>
    (
        *eigenValuesSTPtr_
    );
}

// Eigenvalues tensorField spd test
Foam::GeometricField<Foam::symmTensor, Foam::fvPatchField, Foam::volMesh>
Foam::symmDiagQL::sPDeigenValuesST
(
    const Foam::GeometricField
    <
        Foam::symmTensor, Foam::fvPatchField, Foam::volMesh
    >& A,
    const bool& solve
) const
{
    if (solve)
    {
        // diagonalize A
        symmDiagQL::diag(A, true);
    }
    else if (!eigenValuesSTPtr_)
    {
        WarningIn("Foam::symmDiagQL::sPDeigenValuesST(")
                << "const Foam::GeometricField<"
                << "Foam::symmTensor, Foam::fvPatchField, Foam::volMesh"
                << "& A, const bool& solve) " << endl
                << "No *eigenValuesSTPtr_ found " << endl
                << "Starting diagonalization... " << endl;

        // diagonalize A
        symmDiagQL::diag(A, true);
    }

    return GeometricField<symmTensor, fvPatchField, volMesh>
    (
        *eigenValuesSTPtr_
    );
}

// Eigenvectors tensorField
Foam::GeometricField<Foam::tensor, Foam::fvPatchField, Foam::volMesh>
Foam::symmDiagQL::eigenVectorsT
(
    const Foam::GeometricField
    <
        Foam::symmTensor, Foam::fvPatchField, Foam::volMesh
    >& A,
    const bool& solve
) const
{
    if (solve)
    {
        // diagonalize A
        symmDiagQL::diag(A, false);
    }
    else if (!eigenVectorsTPtr_)
    {
        WarningIn("Foam::symmDiagQL::eigenVectorsT(")
                << "const Foam::GeometricField<"
                << "Foam::symmTensor, Foam::fvPatchField, Foam::volMesh"
                << "& A, const bool& solve) " << endl
                << "No *eigenVectorsTPtr_ found " << endl
                << "Starting diagonalization... " << endl;

        // diagonalize A
        symmDiagQL::diag(A, false);
    }

    return GeometricField<tensor, fvPatchField, volMesh>
    (
        *eigenVectorsTPtr_
    );
}

// Eigenvectors tensorField spd test
Foam::GeometricField<Foam::tensor, Foam::fvPatchField, Foam::volMesh>
Foam::symmDiagQL::sPDeigenVectorsT
(
    const Foam::GeometricField
    <
        Foam::symmTensor, Foam::fvPatchField, Foam::volMesh
    >& A,
    const bool& solve
) const
{
    if (solve)
    {
        // diagonalize A
        symmDiagQL::diag(A, true);
    }
    else if (!eigenVectorsTPtr_)
    {
        WarningIn("Foam::symmDiagQL::sPDeigenVectorsT(")
                << "const Foam::GeometricField<"
                << "Foam::symmTensor, Foam::fvPatchField, Foam::volMesh"
                << "& A, const bool& solve9 " << endl
                << "No *eigenVectorsTPtr_ found " << endl
                << "Starting diagonalization... " << endl;

        // diagonalize A
        symmDiagQL::diag(A, true);
    }

    return GeometricField<tensor, fvPatchField, volMesh>
    (
        *eigenVectorsTPtr_
    );
}

// Free memory
void Foam::symmDiagQL::clearEigenPointers() const
{
    if (debug)
    {
        Info << "(dbginfo):" << tab
             << "Deleting diagonalize pointers ... "<< endl;
    }

    deleteDemandDrivenData(eigenValuesSTPtr_);
    deleteDemandDrivenData(eigenVectorsTPtr_);
}

void Foam::symmDiagQL::clearEigenPointers
(
    const Foam::GeometricField
    <
        Foam::symmTensor,
        Foam::fvPatchField,
        Foam::volMesh
    >& sTD
) const
{
    if (debug)
    {
        Info << "(dbginfo):" << tab
             << "Deleting diagonalize pointers ... "<< endl;
    }

    deleteDemandDrivenData(eigenValuesSTPtr_);
    deleteDemandDrivenData(eigenVectorsTPtr_);
}

// *********************************************************************** //
Foam::GeometricField<Foam::symmTensor, Foam::fvPatchField, Foam::volMesh>
Foam::symmDiagQL::eigenValuesST() const
{
    if (!(eigenValuesSTPtr_))
    {
        FatalErrorIn
        (
            "Foam::symmDiagQL::eigenValuesST()"
        )   << " no eigenValuesSTPtr_ found "
            << nl
            << endl
            << exit(FatalError);
    }

    return *eigenValuesSTPtr_;
}

// *********************************************************************** //
Foam::GeometricField<Foam::tensor, Foam::fvPatchField, Foam::volMesh>
Foam::symmDiagQL::eigenVectorsT() const
{
    if (!(eigenVectorsTPtr_))
    {
        FatalErrorIn
        (
            "Foam::symmDiagQL::eigenVectorsT()"
        )   << " no eigenVectorsTPtr_ found "
            << nl
            << endl
            << exit(FatalError);
    }

    return *eigenVectorsTPtr_;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
