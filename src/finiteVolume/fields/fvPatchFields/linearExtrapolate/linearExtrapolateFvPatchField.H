/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::linearExtrapolateFvPatchField

SourceFiles
    linearExtrapolateFvPatchField.C

Author
    Matthias Niethammer <niethammer@gmx.de>
 
Description
    This boundary condition extrapolates field to the patch using the near-cell
    values.

\*---------------------------------------------------------------------------*/

#ifndef linearExtrapolateFvPatchField_H
#define linearExtrapolateFvPatchField_H

#include "fixedValueFvPatchFields.H"


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                  Class linearExtrapolateFvPatchField Declaration
\*---------------------------------------------------------------------------*/

template<class Type>
class linearExtrapolateFvPatchField
:
    public fixedValueFvPatchField<Type>
{
    // Private data

        //- List to store gradient
        List<tmp<volVectorField> > cmptGrad_;

        //- Name iF
        word iFName_;


public:

    //- Runtime type information
    TypeName("linearExtrapolate");


    // Constructors

        //- Construct from patch and internal field
        linearExtrapolateFvPatchField
        (
            const fvPatch&,
            const DimensionedField<Type, volMesh>&
        );

        //- Construct from patch, internal field and dictionary
        linearExtrapolateFvPatchField
        (
            const fvPatch&,
            const DimensionedField<Type, volMesh>&,
            const dictionary&
        );

        //- Construct by mapping given linearExtrapolateFvPatchField
        //  onto a new patch
        linearExtrapolateFvPatchField
        (
            const linearExtrapolateFvPatchField<Type>&,
            const fvPatch&,
            const DimensionedField<Type, volMesh>&,
            const fvPatchFieldMapper&
        );

        //- Construct as copy
        linearExtrapolateFvPatchField
        (
            const linearExtrapolateFvPatchField<Type>&
        );

        //- Construct and return a clone
        virtual tmp<fvPatchField<Type>> clone() const
        {
            return tmp<fvPatchField<Type>>
            (
                new linearExtrapolateFvPatchField<Type>(*this)
            );
        }

        //- Construct as copy setting internal field reference
        linearExtrapolateFvPatchField
        (
            const linearExtrapolateFvPatchField<Type>&,
            const DimensionedField<Type, volMesh>&
        );

        //- Construct and return a clone setting internal field reference
        virtual tmp<fvPatchField<Type>> clone
        (
            const DimensionedField<Type, volMesh>& iF
        ) const
        {
            return tmp<fvPatchField<Type>>
            (
                new linearExtrapolateFvPatchField<Type>(*this, iF)
            );
        }


    // Member functions

        //- Update the coefficients associated with the patch field
        virtual void updateCoeffs();

        //- Write
        virtual void write(Ostream&) const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#ifdef NoRepository
    #include "linearExtrapolateFvPatchField.C"
#endif

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
