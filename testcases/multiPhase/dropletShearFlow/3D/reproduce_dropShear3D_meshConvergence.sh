#!/bin/bash



Help()
{
   # Display Help
   echo 
   echo " Command to run the case study"
   echo "./reproduce_dropShear3D_meshConvergence.sh "
   echo
}

while getopts ":h" option; do
   case $option in
      h) # display Help
         Help
         exit;;
   esac
done

#VN
cp -r case3D_meshConvergence.parameter case.parameter
cp -r case/system/vofInitDict.VN case/system/vofInitDict.template
cp -r case/constant/transportProperties.OB case/constant/transportProperties.template
cp -r create-study_meshConvergence.py create-study.py

testFileName=test3D_meshConv_VN

#Parametrize  using pyFoam
./create-study.py -s $testFileName -c case -p case.parameter

testFileName=test3D_meshConv
#intialization of cases
for case in $testFileName*; do cd $case; ./Allclean; cd ..; done
for case in $testFileName*; do cd $case; rm -rf 0/*template; cd ..; done
for case in $testFileName*; do cd $case; rm -rf system/*template; cd ..; done
for case in $testFileName*; do cd $case; rm -rf constant/*template; cd ..; done

#for case in $testFileName*; do cd $case; ./Allrun; cd ..; done
for case in $testFileName*; do cd $case; touch case.foam; cd ..; done
#local run
#for case in $testFileName*; do cd $case; interFlow >log.interFlow; cd ..; done

#remote run
#for case in $testFileName*; do cd $case; sbatch script.sh; cd ..; done

#echo $1
#echo $testFileName
