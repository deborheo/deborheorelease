# Droplet shear test case

A single drop deforming under simple shear flow is studied. The test case includes different combinations of the droplet phase and the surrounding fluid matrix. Here, we are interested in the configurations drop/matrix: Newtonian/Newtonian (NN), Newtonian/viscoelastic (NV), and viscoelastic/Newtonian (VN).

In this test case, the drop deformation is studied by means of the deformation parameter

$D = \frac{r_{max} - r_{min}}{r_{max} + r_{min}} $

where $r_{max}$ and $r_{min}$ are the largest and shortest distances from the drop center, respectively. The orientation angle $\theta$ is defined as the angle between the principal axes of the deformed drop and the flow.

# Publication 

[1]  Matthias Niethammer and Muhammad Hassan Asghar and Tomislav Maric and Dieter Bothe [arXiv](
https://doi.org/10.48550/arXiv.2311.10872)

### Compilation & Installation dependencies

Compiler:  g++ (GCC) 9.2.0\
Build system: CMake version 3.26.1
Python 3.9.0

### Computing dependencies

Meshing

- [blockMesh](https://www.openfoam.com/documentation/guides/latest/doc/)

OpenFOAM

To install OpenFOAM from the GitLab repository, follow the [instructions](https://develop.openfoam.com/Development/openfoam)

- Clone the OpenFOAM repository using git.
- Check out the git tag
```
    ?> git checkout OpenFOAM-v2212
```
- Compile OpenFOAM as instructed by its documentation.

DeboRheo

[DeboRheo](https://gitlab.com/deborheo/deborheo) is an open-source computational rheology package designed for solving CFD problems with non-Newtonian, viscoelastic liquids at high Deborah numbers. 

- Compile [DeboRheo](https://gitlab.com/deborheo/deborheo) as instructed by its documentation.

### Case initialization dependencies

[argo](https://gitlab.com/leia-methods/argo/-/tree/master)\
To compute the volume fractions for initialization using the Surface-Mesh/Cell Approximation Algorithm, SMCA, using
exact implicit surfaces.

[PyFoam](https://openfoamwiki.net/index.php/Contrib/PyFoam) - version 2021.6\
To set up parametric studies for verification and validation of the numerical method.

### Post-processing dependencies

We use the data in a `*.csv |*.vtk | *.vtp` file format that is read by [Jupyter notebooks](https://jupyter.org/) for visualization and processing of test results.


## How to set and run the case studies

An executable script and Python modules are currently used to set up and execute parametric studies and visualize the results in Jupyter notebooks.

### How to use the test case

Use the script `reproduce_<test_case>.sh` with `-h` to get further information about the script and the options required to run the case study.

**Example**

- Go to a test case folder, e.g., `2D/`:
```
?> cd 2D
```
- Run the `reproduce_dropShear2D_meshConvergence.sh` script that executes
    - `./create-study.py case case.paramter` to create the case directories of a parametric study
    - `./Allclean` to clean the case directories
    - `./Allrun` to create the mesh and initialize fields in each case directories
    - the solver (local/remote job submission) in each case directory

- The data has been written to a `*.csv |*.vtk | *.vtp` file in each case directory's `postProcessing/` directory.

- The Jupyter notebooks access `postProcessing/` files to visualize and process test results.

#### Initialization scripts

The initialization scripts and their description are as follows:

| Script  | Description |
| ------ | ------ |
| `reproduce_dropShear2D_meshConvergence.sh` | Initialize and run the parametric cases for mesh convergence study (2D/3D). |
| `reproduce_dropShear2D_curvatureComp.sh` | Compare the curvature model available in [TwoPhaseFlow](https://github.com/tmaric/TwoPhaseFlow) library|
| `reproduce_dropShear2D_<dropMatrixConfig>_<rheologicalModel>.sh` | Initialize and run the parametric cases for drop shear <dropMatrixConfig> has NN/NV/VN configurations and <rheologicalModel> has Gi/OB values. Gi represents Giesekus, and OB represents the Oldroyd-B model.  |

Initialization scripts for 3D follow the same numerology.

#### Templated case structure

All the test cases within this study are templated so that the parametric studies can be created from them. So, inside each case study directory, the following files and directories are present, e.g., `2D/`:
- the templated case, namely `case`,
- a parameter file, namely `case.parameter`,
- template files with extension `.template`.

The template files contain parameter placeholders, e.g., `@!De!@` for the Deborah number. These placeholders are replaced by the values from the `case.parameter` file during the parametric study creation (`./create-study.py case case.parameter`).

Additionally, all test case directories contain `variation_file` that contains the information of the variants and their parameter vectors obtained by executing the following:
 ```
?> pyFoamRunParamaterVariation.py --list-variations case case.paramter
```
With this information, the user can also identify the case parametric directory corresponding to a specific parameter vector.

#### Mesh generation and initialization

The `./Allrun` script takes care of mesh generation and field initialization. It

- generates mesh using OpenFOAM's `blockMesh` utility.
- initializes the fields, e.g., volume fraction, using the `setAlphaFieldDict` or the `vofInitDict` dictionary that uses the SMCA initialization algorithm from argo.
- decomposes the domain (if required)

#### Function Object (FO)

Function object [`shearProfile`](../../../src/functionObjects/shearProfile) is used.

| FO  | Description |
| ------ | ------ |
| `shearProfile` | Initialize the shear profile in the computational domain based on the wall velocity $U$ given by the shear rate $\dot{\gamma} = (U_t - U_b) /H$, here $U_t$ and $U_b$ is the wall velocity at the top and bottom wall with domain height $H$. |


#### Running a case study

The script `reproduce_dropShear2D_meshConvergence.sh` executes the solver in all the parametric case directories. If running all parameter cases is not required, the command to run all variations should be omitted before executing the script.
Each case study can be executed in parallel using `mpirun` with `-np,` providing the number of MPI processes.

#### Executing applications with SLURM

The script, namely `script.sh` in each case directory, supports the submission to the SLURM workload manager using the `sbatch` command. This job submission command is also executed within `reproduce_dropShear2D_meshConvergence.sh`, so the user must choose the appropriate command before executing the `reproduce_dropShear2D_meshConvergence.sh` script.

## Post-processing

#### Plots

The notebooks `getCSV_*` reads the `.vtk` files generated from the simulations inside the `postProcessing/surface/` directory of each study, and write the `.csv` files for calculate the drop deformation and deformation angle. Subsequently, the notebooks `Deformation_Notebook_*` generates the figures.

The notebooks are also provided to visualize the drop shape at a specific time.  
