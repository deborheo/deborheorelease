/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Class
    stressAdvection

SourceFiles
    stressAdvection.C

Author
    Matthias Niethammer <niethammer@gmx.de>
 
Description
    Does not solve the constitutive stress equations.
    Pure advection of the initial stress profile.
    Mainly written for testing advection schemes.

\*---------------------------------------------------------------------------*/

#ifndef stressAdvection_H
#define stressAdvection_H

#include "stressRepresentation.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                        Class stressAdvection Declaration
\*---------------------------------------------------------------------------*/
class stressAdvection
:
    public stressRepresentation
{

private:

    // Private data

        //- The complete type name
        word fullTypeName_;

    // Private Member Functions

        //- Correct the stresses (polymer part)
        void correctSinglePhase(const bool& lastIter);

        //- Return the coupling term for the momentum equation
        template<class Type>
        tmp<fvVectorMatrix> divTauT(volVectorField& U, const Type& etaS) const;

        //- Return the coupling term for the momentum equation
        template<class Type>
        tmp<fvVectorMatrix> divTauT(volVectorField& U) const;

        //- Disallow default bitwise copy construct
        stressAdvection(const stressAdvection&);

        //- Disallow default bitwise assignment
        void operator=(const stressAdvection&);


public:

        //- Runtime type information
        TypeName("stressAdvection");


    // Constructors

        //- Construct from components
        stressAdvection
        (
            const word& name,
            const dictionary& stab,
            const dictionary& rheology,
            const volVectorField& U,
            const surfaceScalarField& phi
        );


    // Destructor

        virtual ~stressAdvection()
        {}


    // Member Functions

        //- Return the coupling term for the momentum equation
        virtual tmp<fvVectorMatrix> divTau(volVectorField& U);

        //- Correct the constitutive variable
        virtual void correct(const bool& lastIter);

        //- Correct the viscoelastic stress
        virtual const word& fullTypeName() const;

};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
