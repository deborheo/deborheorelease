/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Niethammer <niethammer@gmx.de>

\*---------------------------------------------------------------------------*/

#include "correctedRepresentation.H"
#include "fv.H"
#include "linear.H"
#include "fvcGrad.H"
#include "gaussGrad.H"
#include "fvMesh.H"
#include "volFields.H"
#include "surfaceFields.H"
#include "demandDrivenData.H"
#include "coupledFvPatch.H"
#include "fvMatrices.H"


// * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * * * //


namespace Foam
{

defineTypeNameAndDebug(correctedRepresentation, 0);

}

void Foam::correctedRepresentation::clearAllData() const
{
    clearData();
    deleteDemandDrivenData(corrGammaSTPtr_);
}

void Foam::correctedRepresentation::clearData() const
{
    deleteDemandDrivenData(correctionVectors_);
    deleteDemandDrivenData(rAAPtr_);
    deleteDemandDrivenData(Sf_);
    //deleteDemandDrivenData(magSf_);
    deleteDemandDrivenData(SfGammaSn_);
}

void Foam::correctedRepresentation::makeGammaST
(
    const Foam::GeometricField<Foam::scalar, Foam::fvPatchField, Foam::volMesh>& rAA
) const
{
    if (!corrGammaSTPtr_)
    {
        corrGammaSTPtr_ = new GeometricField<symmTensor, fvsPatchField, surfaceMesh>
        (
            createCorrGammaST(rAA).cref()
        );
    }
    else
    {
        (*corrGammaSTPtr_) = createCorrGammaST(rAA).cref();
    }

    if (!rAAPtr_)
    {
        rAAPtr_ = new GeometricField<scalar, fvPatchField, volMesh>
        (
            rAA
        );
    }
    else
    {
        (*rAAPtr_) = rAA;
    }
}

void Foam::correctedRepresentation::makeGammaSTH
(
    const Foam::GeometricField<Foam::scalar, Foam::fvPatchField, Foam::volMesh>& rAA
) const
{
    if (!corrGammaSTPtr_)
    {
        corrGammaSTPtr_ = new GeometricField<symmTensor, fvsPatchField, surfaceMesh>
        (
            createCorrGammaSTH(rAA).cref()
        );
    }
    else
    {
        (*corrGammaSTPtr_) = createCorrGammaSTH(rAA).cref();
    }

    if (!rAAPtr_)
    {
        rAAPtr_ = new GeometricField<scalar, fvPatchField, volMesh>
        (
            rAA
        );
    }
    else
    {
        (*rAAPtr_) = rAA;
    }
}

void Foam::correctedRepresentation::makeGammaSTH
(
    const Foam::GeometricField<Foam::scalar, Foam::fvPatchField, Foam::volMesh>& rAA,
    const GeometricField<scalar, fvPatchField, volMesh>& alpha1
) const
{
    if (!corrGammaSTPtr_)
    {
        corrGammaSTPtr_ = new GeometricField<symmTensor, fvsPatchField, surfaceMesh>
        (
            createCorrGammaSTH(rAA, alpha1).cref()
        );
    }
    else
    {
        (*corrGammaSTPtr_) = createCorrGammaSTH(rAA, alpha1).cref();
    }

    if (!rAAPtr_)
    {
        rAAPtr_ = new GeometricField<scalar, fvPatchField, volMesh>
        (
            rAA
        );
    }
    else
    {
        (*rAAPtr_) = rAA;
    }
}

void Foam::correctedRepresentation::makeSf
() const
{
//    Info << max(mag((*corrGammaSTPtr_) & tau_.mesh().Sf())) << endl;
//    Info << max(mag(tau_.mesh().Sf() & (*corrGammaSTPtr_))) << endl;

    if (Sf_)
    {
        (*Sf_) = surfaceVectorField
        (
            (*corrGammaSTPtr_) & tau_.mesh().Sf()
        );
    }
    else
    {
        Sf_ = new surfaceVectorField
        (
            (*corrGammaSTPtr_) & tau_.mesh().Sf()
        );
    }
}


//void Foam::correctedRepresentation::makeMagSf
//() const
//{
//    if (!Sf_)
//    {
//        makeSf();
//    }

//    if (magSf_)
//    {
//        (*magSf_) = surfaceScalarField
//        (
//            mag((*Sf_)) + dimensionedScalar("vs", (*Sf_).dimensions(), VSMALL)
//            //mag((*Sf_)) + (dimensionedScalar("vs", (*Sf_).dimensions()/tau_.mesh().magSf().dimensions(), overrelax_) * tau_.mesh().magSf())
//        );
//    }
//    else
//    {
//        magSf_ = new surfaceScalarField
//        (
//            mag((*Sf_)) + dimensionedScalar("vs", (*Sf_).dimensions(), VSMALL)
//            //mag((*Sf_)) + (dimensionedScalar("vs", (*Sf_).dimensions()/tau_.mesh().magSf().dimensions(), overrelax_) * tau_.mesh().magSf())
//        );
//    }
//}

void Foam::correctedRepresentation::makeSfGammaSn
() const
{
    const fvMesh& mesh = tau_.mesh();
    const surfaceVectorField nf = mesh.Sf()/mesh.magSf();

    if (SfGammaSn_)
    {
        (*SfGammaSn_) = surfaceScalarField
        (
            ic_ * (mesh.Sf() & (*corrGammaSTPtr_))() & nf
        );
    }
    else
    {
        SfGammaSn_ = new surfaceScalarField
        (
            ic_ * (mesh.Sf() & (*corrGammaSTPtr_))() & nf
        );
    }
}




//void Foam::correctedRepresentation::makeDeltaCoeffs
//() const
//{
//    // Force the construction of the weighting factors
//    // needed to make sure deltaCoeffs are calculated for parallel runs.
//    //weights();

//    differenceFactors_ = new surfaceScalarField
//    (
//        IOobject
//        (
//            "differenceFactors("+tau_.name()+')',
//            tau_.mesh().pointsInstance(),
//            tau_.mesh()
//        ),
//        tau_.mesh(),
//        dimless/dimLength
//    );
//    surfaceScalarField& DeltaCoeffs = *differenceFactors_;

//    // Set local references to mesh data
//    const volVectorField& C = tau_.mesh().C();
//    const labelUList& owner = tau_.mesh().owner();
//    const labelUList& neighbour = tau_.mesh().neighbour();
//    const surfaceVectorField& meshSf = tau_.mesh().Sf();
//    const surfaceScalarField meshMagSf = tau_.mesh().magSf();

//    //volVectorField meshNf = meshSf/meshMagSf;

//    forAll (owner, facei)
//    {
//        vector delta = C[neighbour[facei]] - C[owner[facei]];
//        vector unitArea = meshSf[facei]/meshMagSf[facei];

//        DeltaCoeffs[facei] = 1./max(unitArea & delta, 0.05*mag(delta));
//    }

//    forAll (DeltaCoeffs.boundaryField(), patchi)
//    {

//        vectorField delta(tau_.mesh().boundary()[patchi].delta()());
//        vectorField pnf = meshSf.boundaryField()[patchi]/meshMagSf.boundaryField()[patchi];

//        DeltaCoeffs.boundaryFieldRef()[patchi] =
//        (
//            1./max(pnf & delta, 0.05*mag(delta))
//        );
//    }
//}

void Foam::correctedRepresentation::makeCorrectionVectorsD
() const
{
    correctionVectors_ = new surfaceVectorField
    (
        IOobject
        (
            "correctionVectors("+tau_.name()+')',
            tau_.mesh().pointsInstance(),
            tau_.mesh()
        ),
        tau_.mesh(),
        dimless
    );
    surfaceVectorField& corrVecs = *correctionVectors_;

    // Set local references to mesh data
    const volVectorField& C = tau_.mesh().C();
    const labelUList& owner = tau_.mesh().owner();
    const labelUList& neighbour = tau_.mesh().neighbour();
    //const surfaceVectorField& meshSf = tau_.mesh().Sf();
    const surfaceScalarField meshMagSf = tau_.mesh().magSf();

    //const surfaceScalarField& DeltaCoeffs = this->deltaCoeffs();
    const surfaceScalarField& DeltaCoeffs = tau_.mesh().deltaCoeffs();

    forAll (owner, facei)
    {
        vector delta = C[neighbour[facei]] - C[owner[facei]];

        corrVecs[facei] = (-delta*DeltaCoeffs[facei]);
    }

    // Boundary correction vectors set to zero for boundary patches
    // and calculated consistently with internal corrections for
    // coupled patches

    forAll (corrVecs.boundaryField(), patchI)
    {
        vectorField& pCorrVecs = corrVecs.boundaryFieldRef()[patchI];

        if (!corrVecs.boundaryField()[patchI].coupled())
        {
            pCorrVecs = vector::zero;
        }
        else
        {
            const fvMesh& mesh = tau_.mesh();

            vectorField patchDeltas = mesh.boundary()[patchI].delta()();

            scalarField patchDeltaCoeffs = DeltaCoeffs.boundaryField()[patchI];

            forAll(pCorrVecs, patchFacei)
            {
                vector delta = patchDeltas[patchFacei];

                pCorrVecs[patchFacei] = vector(-delta*patchDeltaCoeffs[patchFacei]);
            }
        }
    }
}

Foam::tmp<Foam::fvMatrix<Foam::vector> > Foam::correctedRepresentation::fvmDivTauCorrection
(
    GeometricField<vector, fvPatchField, volMesh>& vf
) const
{

    const fvMesh& mesh = tau_.mesh();

    this->makeSfGammaSn();
    this->makeSf();

    if (mesh.changing() || (!correctionVectors_))
    {
        deleteDemandDrivenData(correctionVectors_);
        Info << "Updating correction vectors" << endl;
        this->makeCorrectionVectorsD();
    }

    const surfaceScalarField& meshDeltaCoeffs = mesh.deltaCoeffs();

    const surfaceVectorField nf = mesh.Sf()/mesh.magSf();
    const surfaceVectorField SfGammaTangent = (*Sf_) - ((*SfGammaSn_)*nf);
    surfaceScalarField magSfGamma = 2* ((*SfGammaSn_) + mag(SfGammaTangent)) / rho();

    tmp<fvMatrix<vector> > tfvm
    (
        new fvMatrix<vector>
        (
            vf,
            meshDeltaCoeffs.dimensions()*(*SfGammaSn_).dimensions()*vf.dimensions()/rho().dimensions()
        )
    );
    fvMatrix<vector>& fvm = tfvm.ref();

    fvm.upper() = meshDeltaCoeffs.internalField()*magSfGamma.internalField(); //meshDeltaCoeffs.primitiveField()*(*SfGammaSn_).primitiveField()/rho().value();
    fvm.negSumDiag();

    forAll(vf.boundaryField(), patchi)
    {
        fvPatchField<vector>& pvf = vf.boundaryFieldRef()[patchi];
        const fvsPatchScalarField& pGamma = magSfGamma.boundaryField()[patchi];
        const fvsPatchScalarField& pDeltaCoeffs =
            meshDeltaCoeffs.boundaryField()[patchi];

        if (pvf.coupled())
        {
            fvm.internalCoeffs()[patchi] =
                pGamma*(-vector(pTraits<vector>::one)*pDeltaCoeffs);
            fvm.boundaryCoeffs()[patchi] =
               -pGamma*(vector(pTraits<vector>::one)*pDeltaCoeffs);
        }
        else
        {
            fvm.internalCoeffs()[patchi] = pGamma*pvf.gradientInternalCoeffs();
            fvm.boundaryCoeffs()[patchi] = -pGamma*pvf.gradientBoundaryCoeffs();
        }
    }

    // Correction
    typedef GeometricField<tensor, fvsPatchField, surfaceMesh> vfSGradType;

    // Surface interpolated vf gradient for the non-orthogonal correction term
    tmp<vfSGradType > tvfSGrad
    (
        new vfSGradType
        (
            IOobject
            (
                "sGrad("+vf.name()+')',
                tau_.time().timeName(),
                tau_.mesh(),
                IOobject::NO_READ,
#               ifndef FULLDEBUG
                    IOobject::NO_WRITE
#               endif
#               ifdef FULLDEBUG
                    IOobject::AUTO_WRITE
#               endif
            ),
            tau_.mesh(),
            vf.dimensions()/dimLength,
            calculatedFvsPatchField<tensor>::typeName
        )
    );

#   if 1
    tvfSGrad.ref() =
        tmp<surfaceInterpolationScheme<tensor> >
        (
            surfaceInterpolationScheme<tensor>::New
            (
                mesh,
                mesh.interpolationScheme
                (
                    "interpolate("+tvfSGrad().name()+')'
                )
            )
        ).ref().interpolate
        (
            Foam::fv::gradScheme<vector>::New
            (
                mesh,
                mesh.gradScheme("divGradCorr("+vf.name()+')')
            )()
            .grad(vf)
        )();
#   else
    // linear interpolate gradient
    tvfSGrad.ref() =
        linear<tensor>(mesh).interpolate
        (
            Foam::fv::gradScheme<vector>::New
            (
                mesh,
                mesh.gradScheme("divGradCorr("+vf.name()+')')
            )()
            .grad(vf)
        )();
#   endif

    // Face-interpolation correction source term
    tmp<GeometricField<vector, fvsPatchField, surfaceMesh> > tCorrectionGamma
    (
        (magSfGamma * (*correctionVectors_))() & tvfSGrad.cref()
    );

    fvm.source() -= mesh.V()*fvc::surfaceIntegrate
    (
        tCorrectionGamma()
    ).cref().primitiveField();

#   if 0
    // Apply the full correction (including explicit terms)
    // Note: The full correction costs a lot of computational resources.
    // For efficiency reasons neglect?
    typedef GeometricField<tensor, fvPatchField, volMesh> vfGradType;
    tmp<vfGradType > tvfGradT
    (
        new vfGradType
        (
            IOobject
            (
                "gradCorr("+vf.name()+')',
                tau_.time().timeName(),
                tau_.mesh(),
                IOobject::NO_READ,
#               ifndef FULLDEBUG
                    IOobject::NO_WRITE
#               endif
#               ifdef FULLDEBUG
                    IOobject::AUTO_WRITE
#               endif
            ),
            tau_.mesh(),
            vf.dimensions()/dimLength,
            calculatedFvPatchField<tensor>::typeName
        )
    );
    tvfGradT.ref() =
        T(
            Foam::fv::gradScheme<vector>::New
            (
                mesh,
                mesh.gradScheme("divGradCorr("+vf.name()+')')
            )()
            .grad(vf)()
        );

    fvm -= volVectorField(tvfGradT() & fvc::div((*Sf_)) / rho());
    fvm -= volVectorField(fvc::div((*Sf_)) & tvfGradT().T() / rho());

    tmp<GeometricField<vector, fvsPatchField, surfaceMesh> > tCorrectionSf
    (
        mesh.Sf() 
      & (
            (tvfSGrad().T() + tvfSGrad())
          & (*corrGammaSTPtr_)
          / rho()
        )()
    );

    dimensionedScalar gOne("gOne", dimless, scalar(1));

    fvm += fvc::div(tCorrectionSf()) + (fvc::reconstruct((*Sf_)) & fvc::laplacian(gOne, U())()/ rho())();
#   endif

//    const surfaceVectorField nf = mesh.Sf()/mesh.magSf();

//    // Face-interpolation correction source term
//    tmp<GeometricField<vector, fvsPatchField, surfaceMesh> > tCorrectionGamma
//    (
//        ((*SfGammaSn_) / rho() * (*correctionVectors_))() & tvfSGrad.cref()
//    );

//    fvm.source() -= mesh.V()*fvc::surfaceIntegrate
//    (
//        tCorrectionGamma()
//    ).cref().primitiveField();

    if (mesh.fluxRequired(vf.name()))
    {
        tfvm.ref().faceFluxCorrectionPtr() = tCorrectionGamma.ptr();
    }

    // Clean up memory
    deleteDemandDrivenData(SfGammaSn_);
    deleteDemandDrivenData(Sf_);

    return tfvm;

}


Foam::tmp<Foam::fvMatrix<Foam::vector> > Foam::correctedRepresentation::fvmDivTauCorrectionRho
(
    GeometricField<vector, fvPatchField, volMesh>& vf
) const
{

    const fvMesh& mesh = tau_.mesh();

    this->makeSfGammaSn();
    this->makeSf();

    if (mesh.changing() || (!correctionVectors_))
    {
        deleteDemandDrivenData(correctionVectors_);
        Info << "Updating correction vectors" << endl;
        this->makeCorrectionVectorsD();
    }

    const surfaceScalarField& meshDeltaCoeffs = mesh.deltaCoeffs();

    const surfaceVectorField nf = mesh.Sf()/mesh.magSf();
    const surfaceVectorField SfGammaTangent = (*Sf_) - ((*SfGammaSn_)*nf);
    surfaceScalarField magSfGamma = 2* ((*SfGammaSn_) + mag(SfGammaTangent));

    tmp<fvMatrix<vector> > tfvm
    (
        new fvMatrix<vector>
        (
            vf,
            meshDeltaCoeffs.dimensions()*(*SfGammaSn_).dimensions()*vf.dimensions()
        )
    );
    fvMatrix<vector>& fvm = tfvm.ref();

    fvm.upper() = meshDeltaCoeffs.internalField()*magSfGamma.internalField();
    fvm.negSumDiag();

    forAll(vf.boundaryField(), patchi)
    {
        fvPatchField<vector>& pvf = vf.boundaryFieldRef()[patchi];
        const fvsPatchScalarField& pGamma = magSfGamma.boundaryField()[patchi];
        const fvsPatchScalarField& pDeltaCoeffs =
            meshDeltaCoeffs.boundaryField()[patchi];

        if (pvf.coupled())
        {
            fvm.internalCoeffs()[patchi] =
                pGamma*(-vector(pTraits<vector>::one)*pDeltaCoeffs);
            fvm.boundaryCoeffs()[patchi] =
               -pGamma*(vector(pTraits<vector>::one)*pDeltaCoeffs);
        }
        else
        {
            fvm.internalCoeffs()[patchi] = pGamma*pvf.gradientInternalCoeffs();
            fvm.boundaryCoeffs()[patchi] = -pGamma*pvf.gradientBoundaryCoeffs();
        }
    }

    // Correction
    typedef GeometricField<tensor, fvsPatchField, surfaceMesh> vfSGradType;

    // Surface interpolated vf gradient for the non-orthogonal correction term
    tmp<vfSGradType > tvfSGrad
    (
        new vfSGradType
        (
            IOobject
            (
                "sGrad("+vf.name()+')',
                tau_.time().timeName(),
                tau_.mesh(),
                IOobject::NO_READ,
#               ifndef FULLDEBUG
                    IOobject::NO_WRITE
#               endif
#               ifdef FULLDEBUG
                    IOobject::AUTO_WRITE
#               endif
            ),
            tau_.mesh(),
            vf.dimensions()/dimLength,
            calculatedFvsPatchField<tensor>::typeName
        )
    );

#   if 1
    tvfSGrad.ref() =
        tmp<surfaceInterpolationScheme<tensor> >
        (
            surfaceInterpolationScheme<tensor>::New
            (
                mesh,
                mesh.interpolationScheme
                (
                    "interpolate("+tvfSGrad().name()+')'
                )
            )
        ).ref().interpolate
        (
            Foam::fv::gradScheme<vector>::New
            (
                mesh,
                mesh.gradScheme("divGradCorr("+vf.name()+')')
            )()
            .grad(vf)
        )();
#   else
    // linear interpolate gradient
    tvfSGrad.ref() =
        linear<tensor>(mesh).interpolate
        (
            Foam::fv::gradScheme<vector>::New
            (
                mesh,
                mesh.gradScheme("divGradCorr("+vf.name()+')')
            )()
            .grad(vf)
        )();
#   endif

    // Face-interpolation correction source term
    tmp<GeometricField<vector, fvsPatchField, surfaceMesh> > tCorrectionGamma
    (
        (magSfGamma * (*correctionVectors_))() & tvfSGrad.cref()
    );

    fvm.source() -= mesh.V()*fvc::surfaceIntegrate
    (
        tCorrectionGamma()
    ).cref().primitiveField();

#   if 0
    // Apply the full correction (including explicit terms)
    // Note: The full correction costs a lot of computational resources.
    // For efficiency reasons neglect?
    typedef GeometricField<tensor, fvPatchField, volMesh> vfGradType;
    tmp<vfGradType > tvfGradT
    (
        new vfGradType
        (
            IOobject
            (
                "gradCorr("+vf.name()+')',
                tau_.time().timeName(),
                tau_.mesh(),
                IOobject::NO_READ,
#               ifndef FULLDEBUG
                    IOobject::NO_WRITE
#               endif
#               ifdef FULLDEBUG
                    IOobject::AUTO_WRITE
#               endif
            ),
            tau_.mesh(),
            vf.dimensions()/dimLength,
            calculatedFvPatchField<tensor>::typeName
        )
    );
    tvfGradT.ref() =
        T(
            Foam::fv::gradScheme<vector>::New
            (
                mesh,
                mesh.gradScheme("divGradCorr("+vf.name()+')')
            )()
            .grad(vf)()
        );

    fvm -= volVectorField(tvfGradT() & fvc::div((*Sf_)));
    fvm -= volVectorField(fvc::div((*Sf_)) & tvfGradT().T());

    tmp<GeometricField<vector, fvsPatchField, surfaceMesh> > tCorrectionSf
    (
        mesh.Sf() 
      & (
            (tvfSGrad().T() + tvfSGrad())
          & (*corrGammaSTPtr_)
        )()
    );

    dimensionedScalar gOne("gOne", dimless, scalar(1));

    fvm += fvc::div(tCorrectionSf()) + (fvc::reconstruct((*Sf_)) & fvc::laplacian(gOne, U())())();
#   endif

    if (mesh.fluxRequired(vf.name()))
    {
        tfvm.ref().faceFluxCorrectionPtr() = tCorrectionGamma.ptr();
    }

    // Clean up memory
    deleteDemandDrivenData(SfGammaSn_);
    deleteDemandDrivenData(Sf_);

    return tfvm;

}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::correctedRepresentation::correctedRepresentation
(
    const word& name,
    const dictionary& stab,
    const dictionary& rheology,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    genericRepresentation(name, stab, rheology, U, phi),
    ic_(0.995),
    overrelax_(VSMALL),
    corrGammaSTPtr_
    (
        new GeometricField<symmTensor, fvsPatchField, surfaceMesh>
        (
            IOobject
            (
                "corrGamma("+tau_.name()+')',
                U.time().timeName(),
                U.mesh(),
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            U.mesh(),
            dimensionedSymmTensor
            (
                "0", tau_.dimensions()*dimTime, pTraits<symmTensor>::zero
            ),
            calculatedFvsPatchField<symmTensor>::typeName
        )
    ),
    rAAPtr_(NULL),
    correctionVectors_(NULL),
    Sf_(NULL),
    SfGammaSn_(NULL)
{
    clearData();
}

Foam::correctedRepresentation::correctedRepresentation
(
    const word& name,
    const dictionary& stab,
    const dictionary& rheology,
    const volScalarField& alpha1,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    genericRepresentation(name, stab, rheology, alpha1, U, phi),
    ic_(0.995),
    overrelax_(VSMALL),
    corrGammaSTPtr_
    (
        new GeometricField<symmTensor, fvsPatchField, surfaceMesh>
        (
            IOobject
            (
                "corrGamma("+tau_.name()+')',
                U.time().timeName(),
                U.mesh(),
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            U.mesh(),
            dimensionedSymmTensor
            (
                "0", tau_.dimensions()*dimTime, pTraits<symmTensor>::zero
            ),
            calculatedFvsPatchField<symmTensor>::typeName
        )
    ),
    rAAPtr_(NULL),
    correctionVectors_(NULL),
    Sf_(NULL),
    SfGammaSn_(NULL)
{
    clearData();
}

Foam::correctedRepresentation::correctedRepresentation
(
    const word& name,
    const dictionary& stab,
    const dictionary& rheology,
    const volVectorField& U,
    const surfaceScalarField& phi,
    const volScalarField& etaS,
    const dimensionedScalar& rho
)
:
    genericRepresentation(name, stab, rheology, U, phi, etaS, rho),
    ic_(0.995),
    overrelax_(VSMALL),
    corrGammaSTPtr_
    (
        new GeometricField<symmTensor, fvsPatchField, surfaceMesh>
        (
            IOobject
            (
                "corrGamma("+tau_.name()+')',
                U.time().timeName(),
                U.mesh(),
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            U.mesh(),
            dimensionedSymmTensor
            (
                "0", tau_.dimensions()*dimTime, pTraits<symmTensor>::zero
            ),
            calculatedFvsPatchField<symmTensor>::typeName
        )
    ),
    rAAPtr_(NULL),
    correctionVectors_(NULL),
    Sf_(NULL),
    SfGammaSn_(NULL)
{
    clearData();
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::correctedRepresentation::~correctedRepresentation()
{
    clearAllData();
}

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

Foam::tmp<Foam::surfaceVectorField > Foam::correctedRepresentation::ddtFluxCorrect
(
    const surfaceVectorField& fluxToCorrect
) const
{
    const fvMesh& mesh = tau_.mesh();

    if (rAAPtr_)
    {
        //return fvc::ddtPhiCorr((*rAAPtr_), (tau_/law_->rho())(), fluxToCorrect);
        return fvc::interpolate((*rAAPtr_))*fvc::ddtCorr((tau_/law_->rho())(), fluxToCorrect);
    }

    // Return a zero field
    return tmp<Foam::surfaceVectorField >
    (
        new surfaceVectorField
        (
            IOobject
            (
                "ddtFluxCorrect("+tau_.name()+')',
                mesh.pointsInstance(),
                mesh,
                IOobject::NO_READ,
#               ifndef FULLDEBUG
                    IOobject::NO_WRITE
#               endif
#               ifdef FULLDEBUG
                    IOobject::AUTO_WRITE
#               endif
            ),
            mesh,
            dimensionedVector
            (
                "0",
                tau_.dimensions()/law_->rho().dimensions()*mesh.Sf().dimensions(),
                pTraits<vector>::zero
            )
        )
    );
}

const Foam::GeometricField<Foam::scalar, Foam::fvPatchField, Foam::volMesh>&
Foam::correctedRepresentation::rAA() const
{
    const fvMesh& mesh = tau_.mesh();

    if (rAAPtr_)
    {
        return (*rAAPtr_);
    }

    // Create a zero field
    rAAPtr_ = new GeometricField<scalar, fvPatchField, volMesh>
    (
        IOobject
        (
            "rAA_("+A_.name()+')',
            mesh.pointsInstance(),
            mesh,
            IOobject::NO_READ,
#           ifndef FULLDEBUG
                IOobject::NO_WRITE
#           endif
#           ifdef FULLDEBUG
                IOobject::AUTO_WRITE
#           endif
        ),
        mesh,
        dimensionedScalar
        (
            "0",
            dimTime,
            pTraits<scalar>::zero
        )
    );

    return (*rAAPtr_);
}

const Foam::surfaceVectorField&
Foam::correctedRepresentation::correctionVectors
() const
{
    if (!correctionVectors_)
    {
        makeCorrectionVectorsD();
    }

    return (*correctionVectors_);
}

bool Foam::correctedRepresentation::read
(
    const dictionary& stab,
    const dictionary& rheology
)
{
    genericRepresentation::read(stab, rheology);

    if (stab.found("overrelax"))
    {
        overrelax_ = readScalar(stab.lookup("overrelax"));
    }

    return true;
}

// ************************************************************************* //
