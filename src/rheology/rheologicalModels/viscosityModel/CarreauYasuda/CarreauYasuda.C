/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Niethammer <niethammer@gmx.de>

\*---------------------------------------------------------------------------*/

#include "CarreauYasuda.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{

namespace ViscosityModels
{
    defineTypeNameAndDebug(CarreauYasuda, 0);

    addToRunTimeSelectionTable
    (
        ViscosityModel, CarreauYasuda, dictionaryUPhi
    );

    addToRunTimeSelectionTable
    (
        ViscosityModel, CarreauYasuda, dictionaryUPhiEta
    );
}

}

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

void Foam::ViscosityModels::CarreauYasuda::calcEta() const
{
    tmp<volScalarField> sr(ViscosityModel::strainRate());

    scalarField& etaI = (*etaPtr_).primitiveFieldRef();
    const scalarField& srI = sr().primitiveField();

    etaI = 
    (
        min
        (
            etaMax_.value(),
            etaInf_.value()
          + (eta0_.value() - etaInf_.value())
          * pow
            (
                scalar(1.) + pow(k_.value()*srI, a_.value())
              , (n_.value() - 1.)/a_.value()
            )
        )
    )();

    forAll((*etaPtr_).boundaryField(), patchI)
    {
        if (!(*etaPtr_).boundaryField()[patchI].coupled())
        {
            scalarField& etaP = (*etaPtr_).boundaryFieldRef()[patchI];
            const scalarField& srP = sr().boundaryField()[patchI];

            etaP = 
            (
                min
                (
                    etaMax_.value(),
                    etaInf_.value()
                  + (eta0_.value() - etaInf_.value())
                  * pow
                    (
                        scalar(1.) + pow(k_.value()*srP, a_.value())
                      , (n_.value() - 1.)/a_.value()
                    )
                )
            )();
        }
    }

    (*etaPtr_).correctBoundaryConditions();
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::ViscosityModels::CarreauYasuda::CarreauYasuda
(
    const word& name,
    const dictionary& dict
)
:
    ViscosityModel(name, dict),
    CarreauYasudaCoeffs_(dict.subDict(typeName + "Coeffs")),
    k_("k", dimTime, CarreauYasudaCoeffs_),
    n_("n", dimless, CarreauYasudaCoeffs_),
    a_("a", dimless, CarreauYasudaCoeffs_),
    eta0_("eta0", etaDimensions_, CarreauYasudaCoeffs_),
    etaInf_("etaInf", etaDimensions_, CarreauYasudaCoeffs_),
    etaMax_("etaMax", etaDimensions_, CarreauYasudaCoeffs_)
{}

Foam::ViscosityModels::CarreauYasuda::CarreauYasuda
(
    const word& name,
    const dictionary& dict,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    ViscosityModel(name, dict, U, phi),
    CarreauYasudaCoeffs_(dict.subDict(typeName + "Coeffs")),
    k_("k", dimTime, CarreauYasudaCoeffs_),
    n_("n", dimless, CarreauYasudaCoeffs_),
    a_("a", dimless, CarreauYasudaCoeffs_),
    eta0_("eta0", etaDimensions_, CarreauYasudaCoeffs_),
    etaInf_("etaInf", etaDimensions_, CarreauYasudaCoeffs_),
    etaMax_("etaMax", etaDimensions_, CarreauYasudaCoeffs_)
{
    if
    (
        (!ViscosityModel::checkDimensions(eta0_))
      ||(!ViscosityModel::checkDimensions(etaInf_))
      ||(!ViscosityModel::checkDimensions(etaMax_))
    )
    {
        FatalErrorIn
        (
            "Foam::fieldEtaModels::CarreauYasuda::"
            "CarreauYasuda(const word& name, const dictionary& dict, "
            "const volVectorField& U, const surfaceScalarField& phi)"
        )   << "    Incompatible dimensions. Aborting..."
            << abort(FatalError);
    }

    if(mag(a_.value()) < SMALL)
    {
        WarningIn
        (
            "Foam::fieldEtaModels::CarreauYasuda::"
            "CarreauYasuda(const word& name, const dictionary& dict, "
            "const volVectorField& U, const surfaceScalarField& phi)"
        )   << "    A zero value for " << a_.name() << " is not allowed."
            << nl << "    Trying to override " << a_.name()
            << " with " << SMALL << endl;

        a_.value() = SMALL;
    }
}

Foam::ViscosityModels::CarreauYasuda::CarreauYasuda
(
    const word& name,
    const dictionary& dict,
    const volVectorField& U,
    const surfaceScalarField& phi,
    const word& etaName
)
:
    ViscosityModel(name, dict, U, phi, etaName),
    CarreauYasudaCoeffs_(dict.subDict(typeName + "Coeffs")),
    k_("k", dimTime, CarreauYasudaCoeffs_),
    n_("n", dimless, CarreauYasudaCoeffs_),
    a_("a", dimless, CarreauYasudaCoeffs_),
    eta0_("eta0", etaDimensions_, CarreauYasudaCoeffs_),
    etaInf_("etaInf", etaDimensions_, CarreauYasudaCoeffs_),
    etaMax_("etaMax", etaDimensions_, CarreauYasudaCoeffs_)
{
    if
    (
        (!ViscosityModel::checkDimensions(eta0_))
      ||(!ViscosityModel::checkDimensions(etaInf_))
      ||(!ViscosityModel::checkDimensions(etaMax_))
    )
    {
        FatalErrorIn
        (
            "Foam::fieldEtaModels::CarreauYasuda::"
            "CarreauYasuda(const word& name, const dictionary& dict, "
            "const volVectorField& U, const surfaceScalarField& phi)"
        )   << "    Incompatible dimensions. Aborting..."
            << abort(FatalError);
    }

    if (mag(a_.value()) < SMALL)
    {
        WarningIn
        (
            "Foam::fieldEtaModels::CarreauYasuda::"
            "CarreauYasuda(const word& name, const dictionary& dict, "
            "const volVectorField& U, const surfaceScalarField& phi)"
        )   << "    A zero value for " << a_.name() << " is not allowed."
            << nl << "    Trying to override " << a_.name()
            << " with " << SMALL << endl;

        a_.value() = SMALL;
    }
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::ViscosityModels::CarreauYasuda::~CarreauYasuda()
{}

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void Foam::ViscosityModels::CarreauYasuda::correct()
{
    makeEtaPtr();

    calcEta();
}

const Foam::ViscosityModel::etaType& 
Foam::ViscosityModels::CarreauYasuda::eta() const
{
    if (ViscosityModel::etaPtr_)
    {
        return (*ViscosityModel::etaPtr_);
    }

    FatalErrorIn
    (
        "const typename Foam::ViscosityModel"
        "::etaType& Foam::ViscosityModels::CarreauYasuda::eta() "
        "const"
    )   << "    etaPtr_ is empty! Need to correct() viscosity."
        << abort(FatalError);

    return (*ViscosityModel::etaPtr_);
}

const Foam::ViscosityModel::etaType& 
Foam::ViscosityModels::CarreauYasuda::eta(const bool& stabilize) const
{
    if (ViscosityModel::etaPtr_)
    {
        // Stabilize for division
        if(stabilize)
        {
            stabilizeEtaPtr();
        }
        return (*ViscosityModel::etaPtr_);
    }

    FatalErrorIn
    (
        "const Foam::ViscosityModel"
        "::etaType& Foam::ViscosityModels::CarreauYasuda::eta() "
        "const"
    )   << "    etaPtr_ is empty! Need to correct() viscosity."
        << abort(FatalError);

    return (*ViscosityModel::etaPtr_);
}


bool Foam::ViscosityModels::CarreauYasuda::read
(
    const dictionary& viscosityProperties
)
{
    ViscosityModel::read(viscosityProperties);

    CarreauYasudaCoeffs_ = viscosityProperties.subDict(typeName + "Coeffs");

    CarreauYasudaCoeffs_.readEntry("k", k_);
    CarreauYasudaCoeffs_.readEntry("n", n_);
    CarreauYasudaCoeffs_.readEntry("a", a_);
    CarreauYasudaCoeffs_.readEntry("eta0", eta0_);
    CarreauYasudaCoeffs_.readEntry("etaInf", etaInf_);
    CarreauYasudaCoeffs_.readEntry("etaMax", etaMax_);

    return true;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
