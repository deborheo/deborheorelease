#!/bin/bash
cd ${0%/*} || exit 1    # run from this directory

export CURRDIR="$PWD"
export SRC=$CURRDIR/src
export APP=$CURRDIR/app

echo Clean interIsoDeboRheoFoam dependencies in the TwoPhaseFlow sub-module 
export TWOPHASEFLOW=$CURRDIR/modules/TwoPhaseFlow
if [ -d "$TWOPHASEFLOW" ]; 
then
    wclean $TWOPHASEFLOW/src/VoF
    wclean $TWOPHASEFLOW/src/surfaceForces
    rm -rf $TWOPHASEFLOW/LIBBIN/*
fi

wclean all
wcleanLnIncludeAll
