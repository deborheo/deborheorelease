#!/bin/bash
cd ${0%/*} || exit 1    # run from this directory

export CURRDIR="$PWD"
export SRC=$CURRDIR/src
export APP=$CURRDIR/applications
export UTILITIES=$CURRDIR/utilities

## TwoPhaseFlow Sub-Module for the interIsoDeboRheoFoam solver

### Update the TwoPhaseFlow module 
git submodule init && git submodule update --recursive --remote

### - Create a LIBBIN folder local to TwoPhaseFlow sub-module
if [ ! -d "$CURRDIR"/modules/TwoPhaseFlow/LIBBIN ];
then 
    mkdir -p "$CURRDIR"/modules/TwoPhaseFlow/LIBBIN
fi

### - Export INCLUDE and LIB folder variables 
### - Build interIsoDeboRheoFoam dependencies into TWOPHASEFLOW_LIBBIN 
### - interIsoDeboRheoFoam links with libraries in TWOPHASEFLOW_LIBBIN
export FOAM_USER_LIBBIN_BACKUP=$FOAM_USER_LIBBIN
export FOAM_USER_LIBBIN=$CURRDIR/modules/TwoPhaseFlow/LIBBIN
echo $FOAM_USER_LIBBIN
wmakeLnInclude modules/TwoPhaseFlow/src
wmake libso modules/TwoPhaseFlow/src/VoF
wmake libso modules/TwoPhaseFlow/src/surfaceForces
export FOAM_USER_LIBBIN=$FOAM_USER_LIBBIN_BACKUP

# Libraries

wmake libso $SRC/finiteVolume
wmake libso $SRC/utilities/eigensolver
wmake libso $SRC/utilities/simpleFlowGenerator
wmake libso $SRC/rheology
wmake libso $SRC/twoPhaseTransportModels

# Solvers

wmake $APP/viscoelasticFoam
wmake $APP/viscoelasticSimpleFoam
wmake $APP/interIsoDeboRheoFoam
wmake $APP/interIsoDeboRheoFoamSubCycle
#wmake $APP/adaptiveViscoelasticSimpleFoam
#wmake $APP/viscoelasticChannelFoamSTB
#wmake $APP/viscoelasticChannelPressureFoamSTB
#wmake $APP/viscoelasticFoam1DSH
#wmake $APP/viscoelasticFoam1DEX

# Function Objects
wmake libso $SRC/functionObjects/shearProfile

# Utilities

#######################################




