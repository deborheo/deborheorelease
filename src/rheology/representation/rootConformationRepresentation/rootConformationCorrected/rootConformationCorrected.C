/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Niethammer <niethammer@gmx.de>

\*---------------------------------------------------------------------------*/

#include "rootConformationCorrected.H"
#include "rootType.H"
#include "addToRunTimeSelectionTable.H"
#include "fixedValueFvPatchField.H"
#include "fixedValueFvsPatchField.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(rootConformationCorrected, 0);

    addToRunTimeSelectionTable
    (
        representation,
        rootConformationCorrected,
        representation
    );

    addToRunTimeSelectionTable
    (
        representation,
        rootConformationCorrected,
        representationE
    );

    addToRunTimeSelectionTable
    (
        representation,
        rootConformationCorrected,
        representationAlpha
    );
}


// * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * * * //

// correctSinglePhase
void Foam::rootConformationCorrected::correctSinglePhase(const bool& lastIter)
{
    // Transform stress tensor to root(C)
    A_ = GeometricField<symmTensor, fvPatchField, volMesh>
    (
        rootType_->transformStressToA(tau_, false, true)()
    );
    A_.correctBoundaryConditions();

    // Create velocity gradient
    tmp<GeometricField<tensor, fvPatchField, volMesh> > tL(createL());

    GeometricField<tensor, fvPatchField, volMesh>& L = tL.ref();

    // Decompose convected derivatives for root(C)-representation
    // and compute the r.h.s. expl. rheol. model terms
    tmp<GeometricField<symmTensor, fvPatchField, volMesh> > rhst
    (
        rootType_->decompose(L, true)
    );

    tL.clear();

    // Assemble AEqn (l.h.s.)
    tmp<fvMatrix<symmTensor> > AEqn
    (
        fvm::ddt(A_)
      + fvm::div(phi(), A_, "div(phi,tau)")
      ==
        rhst.cref()
    );

    rhst.clear();

    if(!lastIter)
    {
        AEqn.ref().relax();
    }
    else
    {
        if (U().mesh().relaxEquation(A_.name()+"Final"))
        {
            const scalar& alpha 
                = U().mesh().equationRelaxationFactor(A_.name()+"Final");

            AEqn.ref().relax(alpha);
        }
        else
        {
            AEqn.ref().relax(1);
        }
    }

    tmp<GeometricField<scalar, fvPatchField, volMesh> > rAA
    (
        1.0/(AEqn.cref().A())
    );

    // Solve AEqn
    AEqn.ref().solve();

    AEqn.clear();

    // transform back
    tau_ = GeometricField<symmTensor, fvPatchField, volMesh>
    (
        rootType_->transformAToStress(A_, true, true)()
    );
    tau_.correctBoundaryConditions();

    // Make gamma coeffs to correct interpolation
    correctGammaSTH(rAA.cref());

    rAA.clear();
} // End correctSinglePhase

// correctTwoPhase
void Foam::rootConformationCorrected::correctTwoPhase(const bool& lastIter)
{
    // Limit VoF phase fraction alpha between 0 and 1
    // Note: this should already be guaranteed by the alpha transport algorithm
    //       (e.g. MULES)
    volScalarField limitedAlpha1 = min(max(alpha1(), scalar(0)), scalar(1));

    // Cells that are almost filled completely are considered as "full", i.e. 1.
    // Note: small deviations from 1 can impact the stress considerably since
    //       we are working here with some root function of the stress. We need
    //       to make sure that the full stress is generated in filled cells with
    //       some small tolerance (0.002) for alpha.
//    forAll(limitedAlpha1, cellI)
//    {
//        if(limitedAlpha1[cellI] > 0.998)
//        {
//            limitedAlpha1[cellI] = 1.;
//        }
//        else if(limitedAlpha1[cellI] < 0.04)
//        {
//            // Quadratically reduce the influence of "empty" cells with some
//            // tolerance for alpha1 for the source term
//            limitedAlpha1[cellI] *= limitedAlpha1[cellI];
//        }
//    }
    limitedAlpha1.correctBoundaryConditions();

    // Transform stress tensor to root(C)
    A_ = GeometricField<symmTensor, fvPatchField, volMesh>
    (
        rootType_->transformStressToA(tau_, false, true)()
    );
    A_.correctBoundaryConditions();

    // Create velocity gradient
    tmp<GeometricField<tensor, fvPatchField, volMesh> > tL(createL());

    GeometricField<tensor, fvPatchField, volMesh>& L = tL.ref();

    // Decompose convected derivatives for root(C)-representation
    // and compute the r.h.s. expl. rheol. model terms
    tmp<GeometricField<symmTensor, fvPatchField, volMesh> > rhst
    (
        rootType_->decompose(L, true)
    );

    tL.clear();

    // Assemble AEqn (l.h.s.)
    tmp<fvMatrix<symmTensor> > AEqn
    (
        fvm::ddt(A_)
      + fvm::div(phi(), A_, "div(phi,tau)")
      ==
        limitedAlpha1*rhst()
    );

    rhst.clear();

    if(!lastIter)
    {
        AEqn.ref().relax();
    }
    else
    {
        if (U().mesh().relaxEquation(A_.name()+"Final"))
        {
            const scalar& alpha 
                = U().mesh().equationRelaxationFactor(A_.name()+"Final");

            AEqn.ref().relax(alpha);
        }
        else
        {
            AEqn.ref().relax(1);
        }
    }

    tmp<GeometricField<scalar, fvPatchField, volMesh> > rAA
    (
        1.0/(AEqn.cref().A())
    );

    // Solve AEqn
    AEqn.ref().solve();

    AEqn.clear();

    // transform back
    tau_ = GeometricField<symmTensor, fvPatchField, volMesh>
    (
        rootType_->transformAToStress(A_, true, true)()
    );

    // Quadratically reduce the influence of "empty" cells with some
    // tolerance for alpha1 for the stress
    forAll(limitedAlpha1, cellI)
    {
        if(limitedAlpha1[cellI] < 0.04)
        {
            tau_[cellI] = limitedAlpha1[cellI]*tau_[cellI];
        }
    }
    tau_.correctBoundaryConditions();

    // Make gamma coeffs to correct interpolation
    correctGammaSTH(rAA.cref(), limitedAlpha1);

    rAA.clear();
} // End correctTwoPhase

// Divergence of the extra stress tensor divided by rho
template<class Type>
Foam::tmp<Foam::fvVectorMatrix>
inline Foam::rootConformationCorrected::divTauT
(
    volVectorField& U, const Type& etaS
) const
{
    if (alpha_ > SMALL)
    {
        // Standard BSD approach with face-interpolation correction
        return tmp<fvVectorMatrix>
        (
            fvc::div(tau_/rho(), "div(tau)")
          + fvmDivTauCorrection(U).cref()
          - fvc::laplacian(etaPEff_/rho(), U, "laplacian(etaPEff,U)")
          + fvm::laplacian((etaPEff_+etaS)/rho(), U, "laplacian(etaPEff+etaS,U)")
        );
    }

    // No BSD
    return tmp<fvVectorMatrix>
    (
        fvc::div(tau_/rho(), "div(tau)")
      + fvmDivTauCorrection(U).cref()
      + fvm::laplacian( etaS/rho(), U, "laplacian(etaS,U)")
    );

}// End divTau

// Divergence of the extra stress tensor divided by rho
template<class Type>
Foam::tmp<Foam::fvVectorMatrix>
inline Foam::rootConformationCorrected::divTauT
(
    volVectorField& U
) const
{
    if (alpha_ > SMALL)
    {
        // Standard BSD approach with face-interpolation correction
        return tmp<fvVectorMatrix>
        (
            fvc::div(tau_/rho(), "div(tau)")
          + fvmDivTauCorrection(U).cref()
          - fvc::laplacian(etaPEff_/rho(), U, "laplacian(etaPEff,U)")
          + fvm::laplacian((etaPEff_)/rho(), U, "laplacian(etaPEff,U)")
        );
    }

    // No BSD
    return tmp<fvVectorMatrix>
    (
        fvc::div(tau_/rho(), "div(tau)")
      + fvmDivTauCorrection(U).cref()
    );

}// End divTau

// Divergence of the extra stress tensor
template<class Type>
Foam::tmp<Foam::fvVectorMatrix>
inline Foam::rootConformationCorrected::divTauT
(
    const surfaceScalarField& alpha1f, volVectorField& U
) const
{
    const surfaceVectorField& Sf = tau_.mesh().Sf();
    const surfaceVectorField SfTauf = (Sf & fvc::interpolate(tau_, "interpolate(tau)").cref()).cref();
    if (alpha_ > SMALL)
    {
        // Standard BSD approach with face-interpolation correction
        return tmp<fvVectorMatrix>
        (
            fvc::div(alpha1f*SfTauf)
          + fvmDivTauCorrectionRho(U).cref()
          - fvc::laplacian(etaPEff_, U, "laplacian(etaPEff,U)")
          + fvm::laplacian((etaPEff_), U, "laplacian(etaPEff,U)")
        );
    }

    // No BSD
    return tmp<fvVectorMatrix>
    (
        fvc::div(alpha1f*SfTauf)
      + fvmDivTauCorrectionRho(U).cref()
    );

}// End divTau

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::rootConformationCorrected::rootConformationCorrected
(
    const word& name,
    const dictionary& stab,
    const dictionary& rheology,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    genericRepresentation(name, stab, rheology, U, phi),
    correctedRepresentation(name, stab, rheology, U, phi),
    rootConformationRepresentation(name, stab, rheology, U, phi),
    rootType_
    (
        Foam::rootType::New
        (
            stab,
            *this
        )
    ),
    fullTypeName_(type() + "_" + rootType_->type())
{
    init();
}

Foam::rootConformationCorrected::rootConformationCorrected
(
    const word& name,
    const dictionary& stab,
    const dictionary& rheology,
    const volScalarField& alpha1,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    genericRepresentation(name, stab, rheology, alpha1, U, phi),
    correctedRepresentation(name, stab, rheology, alpha1, U, phi),
    rootConformationRepresentation(name, stab, rheology, alpha1, U, phi),
    rootType_
    (
        Foam::rootType::New
        (
            stab,
            *this
        )
    ),
    fullTypeName_(type() + "_" + rootType_->type())
{
    init();
}

Foam::rootConformationCorrected::rootConformationCorrected
(
    const word& name,
    const dictionary& stab,
    const dictionary& rheology,
    const volVectorField& U,
    const surfaceScalarField& phi,
    const volScalarField& etaS,
    const dimensionedScalar& rho
)
:
    genericRepresentation(name, stab, rheology, U, phi, etaS, rho),
    correctedRepresentation(name, stab, rheology, U, phi, etaS, rho),
    rootConformationRepresentation(name, stab, rheology, U, phi, etaS, rho),
    rootType_
    (
        Foam::rootType::New
        (
            stab,
            *this
        )
    ),
    fullTypeName_(type() + "_" + rootType_->type())
{
    init();
}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

// Divergence of the extra stress tensor divided by rho
// used in incompressible single-phase solvers to compute the face flux phi
Foam::tmp<Foam::fvVectorMatrix>
Foam::rootConformationCorrected::divTau(volVectorField& U)
{
    if (singlePhase())
    {
        if (alpha_ > SMALL)
        {
            etaPEff_ = alpha_*law_->etaP();
        }

        if (law_->solventActive())
        {
            law_->correctSolvent();

            return divTauT<volScalarField>(U, etaS());
        }

        return divTauT<volScalarField>(U);
    }
    else
    {
        //return divTauT<volScalarField>(U, alpha1());
        if (alpha_ > SMALL)
        {
            etaPEff_ = alpha_*law_->etaP();
        }

        return divTauT<volScalarField>(U);
    }
}

// Divergence of the extra stress tensor
// used in incompressible VoF solvers to compute the face flux phi
Foam::tmp<Foam::fvVectorMatrix>
Foam::rootConformationCorrected::divTau
(
    const surfaceScalarField& alpha1f, volVectorField& U
)
{
    if (alpha_ > SMALL)
    {
        etaPEff_ = alpha_*law_->etaP();
    }
    return divTauT<volScalarField>(alpha1f, U);
}

void Foam::rootConformationCorrected::correct(const bool& lastIter)
{
    if (singlePhase())
    {
        law_->correctPolymer();
        this->correctSinglePhase(lastIter);
        law_->correctThermo();
    }
    else
    {
        law_->correctPolymer();
        this->correctTwoPhase(lastIter);
        law_->correctThermo();
    }
}

bool Foam::rootConformationCorrected::init() const
{
    if (currPattern_ > STANDARD)
    {
        return rootType_->makeCPtr(A_, true, true);
    }
    return false;
}

const Foam::word& Foam::rootConformationCorrected::fullTypeName() const
{
    return fullTypeName_;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
