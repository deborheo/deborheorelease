/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2012                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version         2.0;
    format          ascii;
    class           dictionary;
    location        "system";
    object          controlDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "parametersDict"

application     interIsoDeboRheoFoam;

startFrom       latestTime;

startTime       0.0;

stopAt          endTime;

endTime         15.0;

writeControl    adjustableRunTime;

writeInterval   0.005;

deltaT		1e-5;

purgeWrite      0;

writeFormat     binary;

writePrecision  14;

writeCompression off;

timeFormat      general;

timePrecision   6;

graphFormat     raw;

runTimeModifiable yes;

adjustTimeStep  yes;

maxCo           0.12;

maxAlphaCo      0.12;

maxDeltaT       0.0001;

maxCapillaryNum 2.0; //mu*V/sigma

OptimisationSwitches
{
    //- Parallel IO file handler
    // uncollated (default), collated or masterUncollated
    fileHandler collated;
    maxThreadFileBufferSize 1e9; // the default value is 0 bytes,
    maxMasterFileBufferSize 1e9;  
}

libs
(
    "libDeboRheoFiniteVolume.so"
    "libshearProfileFunctionObject.so"
);

functions
{

    surfaces
    {
        type            surfaces;
        libs
        (
            "libsampling.so"
        );
        writeControl   outputTime;
        //writeInterval  0.0005;
        
        surfaceFormat  vtk;
        formatOptions
        {
            vtk
            {
                legacy true;
                format ascii;
            } 
        }
        fields
        (
            p U alpha.viscoelastic_liquid
        );
        
        interpolationScheme cellPoint;
        
        surfaces
        (
            isoAlpha
            {
                type        isoSurface;
                isoField    alpha.viscoelastic_liquid;
                isoValue    0.5;
                interpolate true;
            }
        );
    }   

    plic
    {
        type            surfaces;
        libs            (geometricVoF postProcess);
        writeControl    writeTime;

        surfaceFormat   vtp;
        fields          (p U);

        interpolationScheme cell;

        surfaces
        {
            freeSurf
            {
                type            interface;
                interpolate     false;
            }
        }
    }
   
    shearProfile
    {
        type shearProfile;
        libs ("libshearProfileFunctionObject.so");
        phaseIndicator alpha.viscoelastic_liquid;
        U ($U 0 0); //value is for y=8mm and 1.98 shear rate
    }
}


// ************************************************************************* //
