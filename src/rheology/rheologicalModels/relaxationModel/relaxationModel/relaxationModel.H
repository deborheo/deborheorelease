/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Class
    relaxationModel

SourceFiles
    relaxationModel.C
    newRelaxationModel.C

Author
    Matthias Niethammer <niethammer@gmx.de>

Description
    Abstract base class for relaxation models

\*---------------------------------------------------------------------------*/

#ifndef relaxationModel_H
#define relaxationModel_H

#include "volFields.H"
#include "surfaceFields.H"
#include "dimensionedTypes.H"
#include "fvc.H"
#include "IOdictionary.H"
#include "runTimeSelectionTables.H"
#include "autoPtr.H"
#include "fieldLambdaType.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                        Class relaxationModel Declaration
\*---------------------------------------------------------------------------*/

class relaxationModel
:
    public fieldLambdaType
{

protected:

    // Static data

        static const dimensionSet lambdaDimensions_;


    // Protected data

        word name_;

        dictionary relaxationProperties_;

        const volVectorField& U_;

        const surfaceScalarField& phi_;

        word lambdaName_;


    // Protected Member Functions

        //- Return true if the dimensionedScalar has lambda dimensions
        bool checkDimensions(const dimensionedScalar&);

        //- Make a new relaxation time pointer
        void makeLambdaPtr() const;

        //- Stabilize the relaxation time for division
        void stabilizeLambdaPtr() const;


private:

    // Private Member Functions

        //- Disallow copy construct
        relaxationModel(const relaxationModel&);

        //- Disallow default bitwise assignment
        void operator=(const relaxationModel&);


public:

    // Public typedefs

        typedef fieldLambdaType::lambdaType_ lambdaType;

    //- Runtime type information
    TypeName("relaxationModel");


    // Declare run-time constructor selection tables

#   ifndef SWIG
        declareRunTimeSelectionTable
        (
            autoPtr,
            relaxationModel,
            dictionary,
            (
                const word& name,
                const dictionary& relaxationProperties
            ),
            (name, relaxationProperties)
        );

        declareRunTimeSelectionTable
        (
            autoPtr,
            relaxationModel,
            dictionaryUPhi,
            (
                const word& name,
                const dictionary& relaxationProperties,
                const volVectorField& U,
                const surfaceScalarField& phi
            ),
            (name, relaxationProperties, U, phi)
        );

        declareRunTimeSelectionTable
        (
            autoPtr,
            relaxationModel,
            dictionaryUPhiN,
            (
                const word& name,
                const dictionary& relaxationProperties,
                const volVectorField& U,
                const surfaceScalarField& phi,
                const word& lambdaName
            ),
            (name, relaxationProperties, U, phi, lambdaName)
        );
#   endif


    // Selectors

        //- Return a reference to the selected relaxation model
        static autoPtr<relaxationModel> New
        (
            const word& name,
            const dictionary& relaxationProperties
        );

        //- Return a reference to the selected relaxation model
        static autoPtr<relaxationModel> New
        (
            const word& name,
            const dictionary& relaxationProperties,
            const volVectorField& U,
            const surfaceScalarField& phi
        );

        //- Return a reference to the selected relaxation model
        static autoPtr<relaxationModel> New
        (
            const word& name,
            const dictionary& relaxationProperties,
            const volVectorField& U,
            const surfaceScalarField& phi,
            const word& lambdaName
        );


    // Constructors

        //- Construct from components
        relaxationModel
        (
            const word& name,
            const dictionary& dict
        );

        //- Construct from components
        relaxationModel
        (
            const word& name,
            const dictionary& dict,
            const volVectorField& U,
            const surfaceScalarField& phi
        );

        //- Construct from components
        relaxationModel
        (
            const word& name,
            const dictionary& dict,
            const volVectorField& U,
            const surfaceScalarField& phi,
            const word& lambdaName
        );


    // Destructor
    virtual ~relaxationModel();


    // Member Functions

        //- Return the name
        const word& name() const
        {
            return name_;
        }

        //- Return the constant reference to the velocity field
        const volVectorField& U() const
        {
            return U_;
        }

        //- Return the phase transport properties dictionary
        const dictionary& relaxationProperties() const
        {
            return relaxationProperties_;
        }

        //- Return the strain rate mag(grad(U))
        tmp<volScalarField> strainRate() const;

        //- Correct nothing
        virtual void correct()
        {}

        //- Replace lambda
        virtual void replaceLambda(const lambdaType& externalLambda)
        {
            if (lambdaPtr_)
            {
                (*lambdaPtr_) = externalLambda;
            }
            else
            {
                lambdaPtr_ = new lambdaType(externalLambda);
            }
            (*lambdaPtr_).correctBoundaryConditions();
        }

        //- Read relaxationProperties dictionary
        virtual bool read(const dictionary& relaxationProperties);
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
