/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | DeboRheo: A computational rheology package
   \\    /   O peration     | for high Deborah number flows.
    \\  /    A nd           |
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) Matthias Niethammer <niethammer@gmx.de>
-------------------------------------------------------------------------------

License
    DeboRheo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DeboRheo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU GENERAL PUBLIC LICENSE
    along with DeboRheo.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Niethammer <niethammer@gmx.de>

\*---------------------------------------------------------------------------*/

//- Return stress tensor representation: implicit part
template<class Type, template<class> class PatchField, class GeoMesh>
Foam::tmp<Foam::fvMatrix<Type> >
Foam::SEPTT::makeTauEqnImplicit
(
    Foam::GeometricField<Type, PatchField, GeoMesh>& tau,
    const Foam::surfaceScalarField& phi
)
{
    return tmp<fvMatrix<Type> >
    (
        fvm::ddt(tau)
      + fvm::div(phi, tau, "div(phi,tau)")
      + fvm::Sp
        (
            (
                (this->rLambda())
              * Foam::exp(epsilon_*(this->lambdaByEtaP())*tr(tau))
            )
            , tau
        )
    );
}

//- Return stress tensor representation: explicit part
template<class Type, template<class> class PatchField, class GeoMesh>
Foam::tmp<Foam::GeometricField<Type, PatchField, GeoMesh> >
Foam::SEPTT::makeTauEqnExplicit
(
    Foam::GeometricField<Type, PatchField, GeoMesh>& tau,
    const Foam::GeometricField<Foam::vector, PatchField, GeoMesh>& U
)
{
    tmp<GeometricField<tensor, PatchField, GeoMesh> > L
    (
        fvc::grad(U, "grad(U)")
    );

    L.ref().correctBoundaryConditions();

    tmp<GeometricField<symmTensor, PatchField, GeoMesh> > C
    (
        twoSymm(tau & L.cref())
    );

    tmp<GeometricField<symmTensor, PatchField, GeoMesh> > twoD
    (
        twoSymm(L.cref())
    );

    L.clear();

    return tmp<GeometricField<Type, PatchField, GeoMesh> >
    (
        ((this->etaPByLambda())*twoD.cref()) + C.cref()
    );
}

//- Return stress tensor representation
template<class Type, template<class> class PatchField, class GeoMesh>
Foam::tmp<Foam::fvMatrix<Type> >
Foam::SEPTT::makeTauEqn
(
    Foam::GeometricField<Type, PatchField, GeoMesh>& tau,
    const Foam::surfaceScalarField& phi,
    const Foam::GeometricField<Foam::vector, PatchField, GeoMesh>& U
)
{
    return tmp<fvMatrix<Type> >
    (
        (
            this->makeTauEqnImplicit(tau, phi)()
        )
        ==
        (
            this->makeTauEqnExplicit(tau, U)()
        )
    );
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
